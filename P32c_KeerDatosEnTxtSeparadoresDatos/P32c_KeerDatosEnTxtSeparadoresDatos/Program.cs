﻿/* 
Leon Oyola, Juan Antonio
P32c) Leer Datos En Fichero.Txt Con Separadores de Campos: 
Realiza un programa que lea el fichero AlumNotas.txt que tienes en la carpeta Datos. 
Se sabe que cada fila contiene los campos: nombre, nota1, nota2 y nota3 separados por ‘;’. 
A partir de estas filas obtenidas, rellena una tabla de string tabAlums y otra de 
float tabNotas de tres columnas. 
A continuación presentar los datos con su cabecera
 Importante: 
 1.	Utilizar Ruta Relativa y mantener la estructura de archivos que se te entrega. 
 2.	El archivo debe estar abierto el menor tiempo posible.
 3.	Se puede utilizar una lista auxiliar pero tienes que actuar como si no se supiera 
    el número de alumnos que guarda el fichero.

    AlumNotasConID

 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

class Program
{
    static void Main(string[] args)
    {

        List<string> datos = LeeFichero();
        string[,] alumnos = new string[datos.Count, 2];
        float[,] notas = new float[datos.Count, 3];
        string[] aux = new string[5];
        bool cID = false;
        int iID = 0;

        Console.Write((int)(datos[0])[0]);

        if ((int)(datos[0])[0] <= (int)("9")[0] && (int)(datos[0])[0] >= (int)("0")[0])
        {
            cID = true;
            iID++;
        }

        for (int i = 0; i < datos.Count - 1; i++)
        {
            aux = datos[i].Split(';');

            alumnos[i, 0] = aux[0];
            if (cID)
                alumnos[i, 1] = aux[1];

            for (int j = 0; j < 3; j++)
                notas[i, j] = (float)Math.Round(Convert.ToDouble(aux[1 + iID + j]), 2);

        }
        Imprime(alumnos, notas);
        Console.WriteLine("\n\n");

        Pausa("salir");

    }

    private static void Imprime(string[,] alumnos, float[,] notas)
    {
        Console.Clear();
        Console.SetCursorPosition(3, 2);
        Console.Write("\tNombre");
        Console.SetCursorPosition(43, 2);
        Console.WriteLine("Prog\tJava\tED\tMedia");
        Console.WriteLine("  -------------------------------------------------------------------");


        for (int i = 0; i < alumnos.GetLength(0) - 1; i++)
        {
            Console.SetCursorPosition(3, 4 + i);
            Console.Write("{0}\t{1}", alumnos[i, 0], alumnos[i, 1]);
            Console.SetCursorPosition(43, 4 + i);
            Console.WriteLine("{0}\t{1}\t{2}\t{3}", notas[i, 0], notas[i, 1], notas[i, 2], Math.Round((notas[i, 0] + notas[i, 1] + notas[i, 2]) / 3, 2));
        }

    }


    private static List<string> LeeFichero()
    {
        List<string> lista = new List<string>();
        StreamReader sr = AbreArchivo();
        string linea;

        do
        {
            linea = sr.ReadLine();
            lista.Add(linea);
        } while (linea != null);
        sr.Close();
        return lista;
    }

    private static StreamReader AbreArchivo()
    {
        bool correcto = false;
        //String rutaRel = @"C:\zDatosPruebas\Datos";
        String directorio = Directory.GetCurrentDirectory();
        int cont = 0;
        string ruta = "";
        for (int i = 0; i < directorio.Length || cont < 2; i++)
        {
            if (directorio[i] == '\\')
                cont++;
            ruta = string.Concat(ruta, directorio[i]);
            if (cont == 3)
                break;
        }
        ruta = string.Concat(ruta, @"zDatosPruebas\Datos");

        // Los datos están en la carpeta Datos, de zDatosPruebas, en el direcotrio principal del usuario actual.


        Console.WriteLine(ruta);

        String archivo;
        StreamReader sr = null;




        do
        {
            Console.Write("fichero: ");
            archivo = Console.ReadLine();
            Console.WriteLine("entrando en: " + ruta + @"\" + archivo + ".txt");

            if (File.Exists(ruta + @"\" + archivo + ".txt"))
            {
                correcto = true;
                sr = (new StreamReader(ruta + @"\" + archivo + ".txt"));
            }
            else
                Pausa("coninuar\n *** Error, fichero no encontrado");


        } while (!correcto);

        return sr;

    }

    private static void Pausa(string texto)
    {
        Console.Write("Pulsa una tecla para " + texto + "\t\t\t");
        Console.ReadKey(true);
    }
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Pausa("continuar\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Pausa("continuar\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

}
