﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace FuncionesFicheros
{
    class Program
    {
        static void Main(string[] args)
        {
            string ruta = CapturaDirectorio();
            StreamReader lee = CapturaFicheroTXT(ruta);

            Console.WriteLine(lee.ReadLine());
            string linea = lee.ReadLine();

            while (linea != null)
            {
                Console.WriteLine(linea);
                linea = lee.ReadLine();
            }
            Console.Write("saliendo...");

            Console.ReadKey(true);
        }

        private static string CapturaDirectorio()
        {
            string ruta;
            bool correcto = false;

            do
            {
                Console.Write("Directorio: ");
                ruta = Console.ReadLine();
                correcto=Directory.Exists(ruta);
               
            } while (!correcto);

            if (ruta[ruta.Length-1] != '\\')
                ruta += '\\';

            return ruta;
        }

        private static StreamReader CapturaFicheroTXT(string ruta)
        {
            string fichero;
            bool correcto = false;

            do
            {
                Console.Write("Fichero: ");
                fichero = Console.ReadLine();
                Console.WriteLine((ruta + fichero + ".txt"));
                correcto = File.Exists((ruta + fichero + ".txt"));
                if (!correcto)
                    Console.WriteLine("El fichero no existe.");
            } while (!correcto);
            
            StreamReader archivo = new StreamReader(ruta + fichero + ".txt");
            return archivo;
        }
    }
}
