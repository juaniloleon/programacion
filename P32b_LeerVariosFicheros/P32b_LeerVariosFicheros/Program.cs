﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text;


class Program
{
    static void Main(string[] args)
    {
        string texto, largo = "";
        int parrafos = 0;
        int tam = 0;
        Console.Write("ruta: ");
        string ruta = Console.ReadLine();
        string fichero;
        do
        {
            texto = "";
            largo = "";
            parrafos = 0;
            tam=0;
            Console.Write("\nnombre del fichero: ");
            fichero = Console.ReadLine();
            if (!File.Exists(ruta + fichero + ".txt"))
                Console.WriteLine("\nERROR [fichero no encontrado]");
            else
            {
                StreamReader lector = new StreamReader(ruta + fichero + ".txt");

                do
                {
                    texto = lector.ReadLine();
                    parrafos++;

                    if (!(texto == null))
                        if (texto.Length > tam)
                        {
                            tam = texto.Length;
                            largo = texto;
                        }


                } while (!(texto == null));
                lector.Close();
                Console.WriteLine("Hay un total de {0} parrafos, el más largo, con {1} caracteres dice: \n {2}", parrafos, tam, largo);

            }

        } while (PreguntaSiNo("\n\n¿desea leer otro archivo?"));
        Console.WriteLine("\n\nSaliendo...");
        Console.ReadKey(true);


    }

    private static bool PreguntaSiNo(string v)
    {
        char c;
        while (true)
        {
            Console.Write(v + " (s/n): ");
            c = Console.ReadKey().KeyChar;
            if (c == 'S' || c == 's')
                return true;
            else if (c == 'N' || c == 'n')
                return false;

            else Console.WriteLine("Error [entrada no admitida]");
        }

    }
}
