/*

COLECCIÓN DE FUNCIONES TW c#
(TopeWapas)

*/


//Matematicas


private static double MediaDeTres(int a, int b, int c){
		double media = (a+b+c)/3.0;
		return Math.Round(media,2);
	}
private static float MediaDeTres(int a, int b, int c){
		float media = (a+b+c)/3.0;
		return Math.Round(media,2);
	}
	
private static double MediaDeN(int[] tabla, int n){
	int total=0;
	for(int i=0; int i<tabla.Lenght; i++)
		total+=tabla[i];
	
	return Math.Round( (double)total/n ,2);
}
private static double MediaDeN(double[] tabla, int n){
	int total=0;
	for(int i=0; int i<tabla.Lenght; i++)
		total+=tabla[i];
	
	return Math.Round(total/n ,2);
}
private static double MediaDeN(float[] tabla, int n){
	int total=0;
	for(int i=0; int i<tabla.Lenght; i++)
		total+=tabla[i];
	
	return Math.Round((double)total/n ,2);
}

public static bool EsPrimo(int num)
    {
        int contador = 2;
        bool primo = true;
        while ((primo) && (contador != num))
        {
            if (num % contador == 0)
                primo = false;
            contador++;
        }
        return primo;
    }
	
private static long Factorial(int num)
    {
        long factorial = 1;
        for (int i = 2; i <= num; i++)
			factorial *= i;
        return factorial;
    }


//Entradas
 private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    
    private static int CapturaEntero(string texto)
    {
        bool esCorrecto;
        int resultado;
        string entrada;
		do
        {
            Console.Write("\n\t{0}: ", texto);
            
			entrada=Console.ReadLine();
			if(entrada!="\n"){			
			
				esCorrecto = Int32.TryParse(entrada, out resultado);
				if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
					Console.WriteLine("\n** Error: Valor introducido no válido **");
			}
			else return -1;
			
        } while (esCorrecto == false);

        return resultado;
    }
	

	
	// de listas
	    private static bool BorraElemento(List<string> lista, string element)
    {
        int id = lista.IndexOf(element);
        if (id==-1)
            return false;

        else lista.RemoveAt(id);
        return true;
    }

    private static bool BorraElemento(List<string> lista, int pos)
    {
        if (lista.Count() > pos && pos >= 0)
            lista.RemoveAt(pos);
        else return false;
        return true;
    }

    private static List<string> IntercambiaPos(List<string> lista, int pos1, int pos2)
    {
        string aux = lista[pos1];
        
        lista.Insert(pos1, lista[pos2]);
        lista.RemoveAt(pos1+1);
        lista.Insert(pos2, aux);
        lista.RemoveAt(pos2 + 1);

        return lista;
    }

    private static List<string> DevuelveListaGente(string[] apellidos, string[] nombres)
    {
        List<string> lista = new List<string>();

        for(int i=0; i<apellidos.Length; i++)
            lista.Add( apellidos[i]+", "+nombres[i]);
                
        return lista;
    }




    }
    private static void MuestraColeccion(List<string> lista, int col)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(col, 5 + i);
            Console.WriteLine(i+") "+lista[i]);
        }

    }


// Salidas
    private static void Pausa(string texto)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        BorraLinea();
        Console.Write("Pulsa una tecla para " + texto+"\t\t\t");
        Console.ForegroundColor = ConsoleColor.White;

        Console.ReadKey(true);
private static void ColocaElNumero(int num, bool color)
    {
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;

        Console.SetCursorPosition(columna, fila);
        Console.Write(num);
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
