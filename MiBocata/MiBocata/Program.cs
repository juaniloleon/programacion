﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiBocata
{
    class Program
    {

        static double[] precios = { 12.5, 8.75, 2.50, 3.50 };
        static void Main(string[] args)
        {
            int tipoProd, cantidad;
            do
            {
                Console.Clear();                    
                tipoProd = EligeProducto();
                cantidad = CapturaEntero("Intruduzca cantidad (en gramos)");


                Console.Write("\t\t El precio de su bocadillo es ");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.Write(CalculaPrecio(tipoProd, cantidad) + " e\n");


                Console.ForegroundColor = ConsoleColor.White;
                Console.BackgroundColor = ConsoleColor.Black;


                Console.Write("\tGracias por su pedido.\n\tNo se atragante con su bocata en lata.");
                Console.ReadKey(true);
            } while (tipoProd!=0);

        }

        private static int EligeProducto()
        {
            int id = 0;
            Console.Write("\n\n");

            Salta();
            Console.WriteLine(" ╔════════════════════════════╗ ");   //Impresion menu
            Salta();
            Console.WriteLine(" ║ ELIGA SU BOCAILLO, ILLO    ║ ");
            Salta();
            Console.WriteLine(" ╠════════════════════════════╣ ");
            Salta();
            Console.WriteLine(" ║ 1- Jamón   (12,5e/Kg)      ║ ");
            Salta();
            Console.WriteLine(" ║                            ║ ");
            Salta();
            Console.WriteLine(" ║ 2- Queso   (8,75e/Kg)      ║ ");
            Salta();
            Console.WriteLine(" ║                            ║ ");
            Salta();
            Console.WriteLine(" ║ 3- Chopepó (2,50e/Kg)      ║ ");
            Salta();
            Console.WriteLine(" ║                            ║ ");
            Salta();
            Console.WriteLine(" ║ 4- Chorizo (3,50e/Kg)      ║ ");
            Salta();
            Console.WriteLine(" ║                            ║ ");
            Salta();
            Console.WriteLine(" ║             +0.48e del pan ║ ");
            Salta();
            Console.WriteLine(" ╚════════════════════════════╝ ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;

            //  id = CapturaEntero("Introduzca ID", 1, 4);
            id = CapturaOpcionPulsada();
            return (id - 1);

            //    char c = Console.ReadKey().KeyChar;

            //    switch (c)
            //    {
            //        case '1':
            //            return 1;
            //        case '2':
            //            return 2;
            //        case '3':
            //            return 3;
            //        case '4':
            //            return 4;
            //    }
            //}while (!correcto);

            //return -1;
        }

        private static int CapturaOpcionPulsada()
        {
            do
            {
                char c = Console.ReadKey(true).KeyChar;

                switch (c)
                {
                    case '1':
                        return 1;
                    case '2':
                        return 2;
                    case '3':
                        return 3;
                    case '4':
                        return 4;
                    default:
                        Console.WriteLine("opción inválida");
                        break;            
                }
            } while (true);
        }

        private static void Salta()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("\t\t");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.BackgroundColor = ConsoleColor.Gray;
        }

        private static double CalculaPrecio(int tipoProd, int cantidad)
        {

            return (Math.Round((cantidad * Precios[tipoProd] / 1000), 2) + 0.48);
        }


        private static int CapturaEntero(string texto)
        {
            bool esCorrecto;
            int resultado;
            string entrada;
            do
            {
                Console.Write("\n\t{0}: ", texto);

                entrada = Console.ReadLine();
                if (entrada != "\n")
                {

                    esCorrecto = Int32.TryParse(entrada, out resultado);
                    if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                        Console.WriteLine("\n** Error: Valor introducido no válido **");
                    else if (resultado < 0)
                    {
                        Console.WriteLine("\n** Error: no puedes pedir negativos **");
                        esCorrecto = false;
                    }
                }
                else return -1;

            } while (esCorrecto == false);

            return resultado;
        }

        private static int CapturaEntero(string texto, int min, int max)
        {
            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (resultado < min || resultado > max)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            return resultado;
        }

    }
}
