﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

/*
 * Alumno: Leon Oyola, Juan Antonio
 * 
 P31b_GuardarNMultiplosDesde: 
Construye un método de nombre NMultiplosDesdeque...
Recibe num, cantidad, numDesde
1)Pide un numero de dos cifras (num), la cantidad y el numero desde donde empezar, lo pasa a la función

2)
Pide el nombre del fichero en el que guardará la tabla
3)
Por último guardará todos los valores separados entre sí por el carácter ‘;’ (punto y coma)
*/


namespace P31b_GuardarNMultiplosDesde
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = CaputuraEntero("Número", 10, 99);
            int cantidad = CaputuraEntero("Cantidad de múltiplos");
            int numDesde = CaputuraEntero("Número de inicio", num);

            int[] tabla = NMultiplosDesde(num, cantidad, numDesde);

            Console.Write("Nombre del fichero de salida: ");
            string fichero = Console.ReadLine();
            StreamWriter file = File.AppendText(@"C:\\zDatosPruebas\\" + fichero + ".txt");

            file.WriteLine("Los {0} primeros multiplos de {1} a partir de {2}:", cantidad, num, numDesde);
            for (int i = 0; i < cantidad; i++)
                file.Write(tabla[i] + ";");
            file.WriteLine();
            file.Close();
            Pausa("salir");

        }

        private static int[] NMultiplosDesde(int num, int cantidad, int numDesde)
        {
            int[] tabla = new int[cantidad];
            int primero;

            primero = num * (numDesde / num) + num;


            tabla[0] = primero;

            for (int i = 1; i < cantidad; i++)

                tabla[i] = tabla[i - 1] + num;



            return tabla;
        }

        private static int CaputuraEntero(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;

            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("{0}: ", texto);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                {

                    Pausa("continuar [valor no valido]\n");

                }

            } while (esCorrecto == false);

            Console.ForegroundColor = ConsoleColor.White;

            return resultado;
        }

        private static int CaputuraEntero(string texto, int min, int max)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;

            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("{0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                {

                    Pausa("continuar [valor no valido]\n");

                }
                else if (resultado < min || resultado > max)
                {

                    Pausa("continuar [valor fuera de rango]\n");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            Console.ForegroundColor = ConsoleColor.White;

            return resultado;
        }

        private static int CaputuraEntero(string texto, int min)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;

            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("{0} [>{1}]: ", texto, min);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                {

                    Pausa("continuar [valor no valido]\n");

                }
                else if (resultado < min)
                {

                    Pausa("continuar [valor fuera de rango]\n");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            Console.ForegroundColor = ConsoleColor.White;

            return resultado;
        }

        private static void Pausa(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("--->  Pulsa una tecla para " + texto);
            Console.ForegroundColor = ConsoleColor.White;

            Console.ReadKey(true);


        }
    }
}
