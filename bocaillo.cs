using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiBocata
{
    class Program
    {

        static double[] Precios = { 12.5, 8.75, 2.50, 3.50 };
        static void Main(string[] args)
        {

            int tipoProd = EligeProducto();
            int cantidad = CapturaEntero("Intruduzca cantidad (en gramos): ");

            Console.Write("El precio de su bocadillo es" + CalculaPrecio(tipoProd, cantidad) +"e\n");
           
            
            Console.Write("\tGracias por su pedido.\n\tNo se atragante con su bocata en lata.");
            Console.ReadKey(true);
        }

        private static int EligeProducto()
        {
            int id = 0;
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("\n\n\t\t╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("\t\t║ ELIGA SU BOCAILLO, ILLO    ║");
            Console.WriteLine("\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t║ 1- Jamón   (12,5e/Kg)      ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 2- Queso   (8,75e/Kg)      ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 3- Chopepó (2,50e/Kg)      ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 4- Chorizo (3,50e/Kg)      ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║   +0.48e del pan           ║");
            Console.WriteLine("\t\t╚════════════════════════════╝");
            Console.ForegroundColor = ConsoleColor.White;

            id = CapturaEntero("Introduzca ID", 1, 4);
            return (id - 1);

            //    char c = Console.ReadKey().KeyChar;

            //    switch (c)
            //    {
            //        case '1':
            //            return 1;
            //        case '2':
            //            return 2;
            //        case '3':
            //            return 3;
            //        case '4':
            //            return 4;
            //    }
            //}while (!correcto);

            //return -1;
        }

        private static double CalculaPrecio(int tipoProd, int cantidad) {
            return ( cantidad * Precios[tipoProd]/1000 )+0.48;
        }


        private static int CapturaEntero(string texto)
        {
            bool esCorrecto;
            int resultado;
            string entrada;
            do
            {
                Console.Write("\n\t{0}: ", texto);

                entrada = Console.ReadLine();
                if (entrada != "\n")
                {

                    esCorrecto = Int32.TryParse(entrada, out resultado);
                    if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                        Console.WriteLine("\n** Error: Valor introducido no válido **");
                }
                else return -1;

            } while (esCorrecto == false);

            return resultado;
        }

        private static int CapturaEntero(string texto, int min, int max)
        {
            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (resultado < min || resultado > max)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            return resultado;
        }

    }
}
