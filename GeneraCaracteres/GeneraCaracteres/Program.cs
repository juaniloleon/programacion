﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneraCaracteres
{

    class Program
    {

        static char[] MAYUS = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N','O','P','Q','R','S','T','U','V','X','Y','Z' };
        static char[] MINUS = { 'a', 'b', 'c', 'd', 'e', 'f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z' };
        static Random rnd = new Random();


        static void Main(string[] args)
        {
            string nombre= generaNombre(5);


            Console.WriteLine(nombre);
            Console.ReadLine();               
        }

        private static string generaNombre(int v)
        {
            String nombre="";

            nombre+= MAYUS[rnd.Next(0, MAYUS.Length)];
            for (int i = 0; i < rnd.Next(4, 11); i++)
            {
                nombre+= MINUS[rnd.Next(0, MINUS.Length)];
            }
            nombre += (" "+MAYUS[rnd.Next(0, MAYUS.Length)]);
            for (int i = 0; i < rnd.Next(4, 11); i++)
            {
                nombre += MINUS[rnd.Next(0, MINUS.Length)];
            }
            nombre += (", " + MAYUS[rnd.Next(0, MAYUS.Length)]);
            for (int i = 0; i < rnd.Next(4, 11); i++)
            {
                nombre += MINUS[rnd.Next(0, MINUS.Length)];
            }

            return nombre;
        }
    }
}
