﻿/*

P27d_BuscaEnTabla

recibe una tabla de doubles tD y un double num, devuelve una lista con todas las posiciones donde aparece

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        double num;
        string entrada;
        bool continuar = true;
        List<int> lista = new List<int>();
        int tam = CapturaEntero("Introduzca tamaño de la tabla", 5, 100);

        double[] tNotas = CargaTablaNotas(tam);

        Pausa("mostrar notas");
        MuestraNotas(tNotas, 2);

        do
        {
            BorraColumna(15, 3, lista.Count());
            BorraLinea(3, 2);
            Console.SetCursorPosition(3, 2);
            lista.Clear();
            Console.Write("Introduzca valor a buscar: ");
            entrada = Console.ReadLine();
            if (entrada != "")
            {
                num = CapturaDouble(0, 10, entrada);
                if (num != -1)
                {
                    lista = BuscaEnTabla(tNotas, num);

                    if (lista.Count != 0)
                    {
                        MuestraResultados(lista, 15);
                        Pausa("continuar");
                    }

                    else
                    {
                        Console.SetCursorPosition(3, 3);
                        Console.Write("\tla nota {0} no existe en la tabla", num);
                        Pausa("continuar");
                        BorraLinea(3, 3);
                    }


                }
                else
                    BorraLinea(3, 2);



            }
            else continuar = false;

        } while (continuar);

        BorraLinea(3, 3);
        Pausa("salir");
    }

    private static void MuestraNotas(double[] tNotas, int numColum)
    {
        for (int i = 0; i < tNotas.Length; i++)
        {
            if (tNotas[i] < 5)
                Console.ForegroundColor = ConsoleColor.Red;

            Console.SetCursorPosition(numColum, 5 + i);
            Console.WriteLine(i + ") " + tNotas[i]);
            Console.ForegroundColor = ConsoleColor.White;

        }


    }
    private static void MuestraResultados(List<int> lista, int numColum)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(numColum, 5 + i);
            Console.WriteLine(lista[i]);
        }

    }

    private static List<int> BuscaEnTabla(double[] tD, double num)
    {
        List<int> lista = new List<int>();

        for (int i = 0; i < tD.Length; i++)
            if (tD[i] == num)
                lista.Add(i);

        return lista;
    }

    private static double[] CargaTablaNotas(int tamTabla)
    {
        Random rnd = new Random();
        double[] tabla = new double[tamTabla];
        for (int i = 0; i < tabla.GetLength(0); i++)
            tabla[i] = Math.Round((rnd.NextDouble() * 10), 1);

        return tabla;
    }

    private static double CapturaDouble(int min, int max, string entrada)
    {
        Console.ForegroundColor = ConsoleColor.Magenta;

        bool esCorrecto;
        double resultado;

        BorraLinea(3, 2);
        esCorrecto = double.TryParse(entrada, out resultado);
        if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
        {
            BorraLinea(3, 2);
            Pausa("continuar [valor no valido - Usa x,y ]");
            return -1;

        }
        else if (resultado < min || resultado > max)
        {
            BorraLinea(3, 2);

            Pausa("continuar [valor fuera de rango]");
            esCorrecto = false;
            return -1;
        }

        Console.ForegroundColor = ConsoleColor.White;
        return resultado;
    }


    private static int CapturaEntero(string texto, int min, int max)
    {
        Console.ForegroundColor = ConsoleColor.Magenta;

        bool esCorrecto;
        int resultado;
        do
        {
            BorraLinea(3, 2);
            Console.Write("{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
            {
                BorraLinea(3, 2);

                Pausa("continuar [valor no valido]");

            }
            else if (resultado < min || resultado > max)
            {
                BorraLinea(3, 2);

                Pausa("continuar [valor fuera de rango]");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        Console.ForegroundColor = ConsoleColor.White;

        return resultado;
    }
    private static void BorraLinea(int i, int j)
    {
        Console.SetCursorPosition(i, j);
        Console.Write("\t\t\t\t\t\t\t\t\t\t\t");
        Console.SetCursorPosition(i, j);

    }
    private static void Pausa(string texto)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        BorraLinea(3, 2);
        Console.Write("Pulsa una tecla para " + texto + "\t\t\t");
        Console.ForegroundColor = ConsoleColor.White;

        Console.ReadKey(true);


    }

    private static void BorraColumna(int col, int num, int largo)
    {
        for (int i = 0; i < largo; i++)
        {
            Console.SetCursorPosition(col, 5 + i);
            for (int j = 0; j <= num; j++)
                Console.Write(" ");
        }

    }

}

