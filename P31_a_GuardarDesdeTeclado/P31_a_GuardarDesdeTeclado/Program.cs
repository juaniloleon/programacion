﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace P31_a_GuardarDesdeTeclado
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena;
            StreamWriter sW = File.CreateText(@"C:\\zDatosPruebas\\P31 Salida.txt");
            do
            {
                Console.Write("Introduzca frase a guardar: ");
                cadena = Console.ReadLine();
                if (cadena != "fin")
                {
                    sW.WriteLine(cadena);
                    Console.WriteLine("\tAlmacenado.");

                }
            } while (cadena != "fin");
            
            sW.Close();
            Console.Write("Saliendo...");

            Console.ReadKey(true);

        }
    }
}

