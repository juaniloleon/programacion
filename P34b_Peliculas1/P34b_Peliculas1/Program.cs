﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


class Program
{
    static void Main(string[] args)
    {
        string[,] datos = LeeFichero("pelis.txt");
        string[,] pelis = generaTabla(datos);

        Console.WriteLine("    Nº\t\tPelicula\t\t\tFormato     Tamaño Valoración");

        for (int k = 0; k < pelis.GetLength(0); k++)
        {
            Console.SetCursorPosition(4,k + 2);
            Console.Write(k+1 + ")  ");
            if (k < 9)
                Console.Write(" ");
            if (k < 99)
                Console.Write(" ");
            Console.Write(pelis[k, 0]);

            Console.SetCursorPosition(50,k + 2);
            Console.Write(pelis[k, 1]);

            Console.SetCursorPosition(60,k + 2);
            Console.Write(pelis[k, 2]);

            Console.SetCursorPosition(71,k + 2);
            Console.Write(pelis[k, 3]);

            Console.WriteLine();
        }

        GrabaFichero(pelis);

        Console.ReadKey();


    }

    private static void GrabaFichero(string[,] pelis)
    {
        string directorio = Directory.GetCurrentDirectory();
        string[] aux = directorio.Split('\\');
        directorio = "";
        for (int i = 0; i < aux.Length - 2; i++)
            directorio += aux[i] + '\\';

        StreamWriter sw = new StreamWriter(directorio + "Datos\\Peliculas_S.txt");

        sw.WriteLine("Nº\t\tPelicula\t\t\tFormato Tamaño\t\tValoración");
        for (int k=0; k<pelis.GetLength(0); k++)
        {
            sw.Write(k + ")  ");
            if (k <= 9)
                sw.Write(" ");
            if (k <= 99)
                sw.Write(" ");
            sw.Write(enTamano(pelis[k, 0],40)+"  ");
            sw.Write(pelis[k, 1]+"\t");
            sw.Write(pelis[k, 2] + "\t\t");
            sw.Write(pelis[k, 3]);
            sw.WriteLine();
            
        }

        sw.Close();
      }

    private static string enTamano(string texto, int tam)
    {
        while (texto.Length < 40)
            texto += " ";
        return texto;
    }

    private static string[,] generaTabla(string[,] datos)
    {
        string[,] pelis = new string[datos.GetLength(0), 4];
        string[] aux = new String[3];
        for (int i = 0; i < datos.GetLength(0); i++)
        {
            aux = datosTitulo(datos[i, 1]);
            pelis[i, 0] = aux[0];
            pelis[i, 1] = aux[1];
            pelis[i, 2] = tamGb(datos[i, 0]);
            pelis[i, 3] = aux[2];
        }

        return pelis;
    }

    private static string[] datosTitulo(string titulo)
    {
        string formato = "";
        string valoracion = "";
        string nombre = "";
        string[] peli = new string[3];

        int aux = titulo.LastIndexOf('.');
        int bux = titulo.LastIndexOf(')');

        formato = titulo.Substring(aux+1);
        aux = titulo.LastIndexOf('(')+1;
        valoracion = titulo.Substring(aux, (bux - aux));
        nombre = titulo.Substring(0, aux-1);

        peli[0] = nombre;
        peli[1] = formato.ToUpper();
        peli[2] = valoracion;

        return peli;


    }



    private static string tamGb(string v)
    {
        string tam = "";
        string[] aux = v.Split('.');

        for (int i = 0; i < aux.Length; i++)
        {
            tam += aux[i];
        }

        double peso = Math.Round(Convert.ToDouble(tam) / (1024 * 1024 * 1024), 2);

        return Convert.ToString(peso) + "Gb";

    }

    private static string[,] LeeFichero(string fichero)
    {
        string[,] tabDatos = null;
        List<string> lista = new List<string>();
        string directorio = Directory.GetCurrentDirectory();
        string[] aux = directorio.Split('\\');
        directorio = "";
        for (int i = 0; i < aux.Length - 2; i++)
            directorio += aux[i] + '\\';

        if (!File.Exists(directorio + "Datos\\" + fichero))
            Console.Write("Archivo no encontrado");
        else
        {
            StreamReader sr = new StreamReader(directorio + "Datos\\" + fichero, Encoding.Default);

            while (!sr.EndOfStream)
                lista.Add(sr.ReadLine());

            sr.Close();
            tabDatos = new string[lista.Count, 2];

            for (int k = 0; k < lista.Count; k++)
            {
                tabDatos[k, 0] = lista[k].Substring(17, 18);
                tabDatos[k, 1] = lista[k].Substring(36);
            }


        }
        return tabDatos;

    }
    private static void Pausa(string texto)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("Pulsa una tecla para " + texto + "\t\t\t");
        Console.ForegroundColor = ConsoleColor.White;

        Console.ReadKey(true);
    }
}

