﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
     
        string[] tabla = LeeFichero();
        
        ImprimeColumnas(tabla, 5);

        Pausa("salir");


    }

    private static void ImprimeColumnas(string[] tabla, int col)
    {
        Console.Clear();
        Console.Write("Valores: \t");

        for (int i = 0; i < tabla.Length-1; i++)
        {
            if ((i + 1) % col == 0)
                Console.WriteLine();
            Console.Write("{0}) {1}\t", i + 1, tabla[i]);

            if (i+1 < 100)
                Console.Write("\t");

        }
        Console.WriteLine("\n\n");

    }

    private static string[] LeeFichero()
    {
       
        StreamReader sr = AbreArchivo();
        string linea = sr.ReadLine();
        sr.Close();
        return linea.Split(';');
    }

    private static StreamReader AbreArchivo()
    {
        bool correcto = false;
        String ruta = Directory.GetCurrentDirectory();
        ruta = string.Concat(ruta, "\\Datos");

        String archivo;
        StreamReader sr = null;
        do
        {
            Console.Write("fichero: ");
            archivo = Console.ReadLine();

            if (File.Exists(ruta + @"\" + archivo + ".txt"))
            {
                correcto = true;
                sr = (new StreamReader(ruta + @"\" + archivo + ".txt"));
            }
            else
                Pausa("coninuar\n *** Error, fichero no encontrado");


        } while (!correcto);

        return sr;

    }

    private static void Pausa(string texto)
    {
        Console.Write("Pulsa una tecla para " + texto + "\t\t\t");
        Console.ReadKey(true);
    }
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Pausa("continuar\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Pausa("continuar\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }


}

