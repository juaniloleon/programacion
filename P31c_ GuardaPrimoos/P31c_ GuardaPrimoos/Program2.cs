﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;



/*P31c_GuardaPrimos
ALUMNO:     Juan Antonio Leon Oyola

Construye elmétodo ListaDePrimosque
    Recibe: un entero numMaximo
    Hace: Construye una lista con todos los números primos anteriores numMaximo
    Devuelve: la lista construida

Construye el método EsPrimoque:
    Recibe: un entero num
    Hace: Averigua si numes primo o no (pista: un número es primo si no es divisible por ninguno de sus primos anteriores)
    Devuelve: truesi es Primo o falsesi no lo es

    El Main:Se le pregunta al usuario cuál es el valor máximo vMaxa comprobar [10...10000]
    Guardará en una lista todos los números primos menores de vMaxLuego guardará esa lista en un fichero primos
    txt separando los valores por ‘;’ */

namespace P31c_GuardaPrimoos
{
    class Program
    {
        static void Main(string[] args)
        {

            int vMax = CaputuraEntero("Valor vMax a comprobar ", 10, 1000000);

            List<int> lista = ListaDePrimos(vMax);

            Anyade("primos", lista);
            Pausa("salir");
        }

        private static List<int> ListaDePrimos(int vMax)
        {
            List<int> lista = new List<int>();

            for (int i = 2; i < vMax; i++)
                if (EsPrimo(i))
                    lista.Add(i);

            return lista;

        }

        public static bool EsPrimo(int num)
        {


            if (num == 2)
                return true;
            if (num % 2 == 0)
                return false;

            for (int i = 3; i < num / i; i += 2)
                if (num % i == 0)
                    return false;
            return true;

        }

        private static void Anyade(string fichero, List<int> lista)
        {
            StreamWriter file = File.AppendText(@"C:\\zDatosPruebas\\" + fichero + ".txt");


            for (int i = 0; i < lista.Count; i++)
                file.Write(lista[i] + ";");
            file.WriteLine();
            file.Close();
        }

        private static int CaputuraEntero(string texto, int min, int max)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;

            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("{0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                {

                    Pausa("continuar [valor no valido]\n");

                }
                else if (resultado < min || resultado > max)
                {

                    Pausa("continuar [valor fuera de rango]\n");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            Console.ForegroundColor = ConsoleColor.White;

            return resultado;
        }

        private static void Pausa(string texto)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("--->  Pulsa una tecla para " + texto);
            Console.ForegroundColor = ConsoleColor.White;

            Console.ReadKey(true);


        }

    }
}
