﻿/*

Alumno: Juan A. Leon
Leer Datos En Fichero.Txt Con Campos dimensionados: 
Repetir la práctica LeerDatosEnTxtConSeparadores pero obteniendo los datos del 
fichero AlumNotasId_CD.txt, sabiendo que los tamaños de cada campo son los siguientes:
  id: 3;   alumno: 28;   notaProg: 3;   notaED: 3;   notaBD: 3;
 * 
Es decir, el enunciado sería:
Realiza un programa que lea el fichero AlumNotasId_CD.txt que tienes en la carpeta Datos. 
Se sabe que cada fila contiene los campos: id, nombre, nota1, nota2 y nota3 
a cada uno de los cuales se les ha dado el siguiente tamaño:
            id: 3;   alumno: 28;   notaProg: 3;   notaED: 3;   notaBD: 3;
 
A partir de estas filas obtenidas, rellena una tabla de string tabAlums y otra de 
float tabNotas de tres columnas. 
A continuación presentar los datos con su cabecera
 Importante: 
 1.	Utilizar Ruta Relativa y mantener la estructura de archivos que se te entrega. 
 2.	El archivo debe estar abierto el menor tiempo posible.
 3.	Se puede utilizar una lista auxiliar pero tienes que actuar como si no se supiera 
    el número de alumnos que guarda el fichero.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LeerDatosEnTxtCamposDimensionados
{
    class Program
    {
        static string[] tabAlumnos;
        static float[,] tabNotas;
        static int[] tabID;

        static void Main(string[] args)
        {
            LeeFichero("AlumNotasID_CD.txt");
            ImprimeteNotas();

            Console.WriteLine("\n\tPulsa cualquier tecla para salir...");
            Console.ReadKey();
        }

        private static void ImprimeteNotas()
        {
            float media;

            Console.SetCursorPosition(3, 1);

            Console.WriteLine("id\tAlumno\t\t\t\tProg\tED\tBD\tMedia");

            for (int i = 0; i < tabAlumnos.GetLength(0); i++)
            {
                Console.SetCursorPosition(3, (2 + i));

                media = 0;
                Console.Write("{0})\t{1}", tabID[i], tabAlumnos[i]);

                for (int j = 0; j < tabNotas.GetLength(1); j++)
                {
                    Console.SetCursorPosition(40 + (8 * j), (2 + i));
                    Console.Write("{0}\t", tabNotas[i, j]);
                    media += tabNotas[i, j];
                }
                Console.WriteLine(Math.Round(media / 3, 2));
            }


        }

        private static void LeeFichero(string fichero)
        {
            List<string> lista = new List<string>();
            string directorio = Directory.GetCurrentDirectory();
            string[] aux=directorio.Split('\\');
            directorio = "";
            for (int i = 0; i < aux.Length - 2; i++)
                directorio += aux[i] + '\\';

            if (!File.Exists(directorio + "Datos\\" + fichero))
                Console.Write("Archivo no encontrado");
            else{
                StreamReader sr = new StreamReader(directorio + "Datos\\" + fichero);

                while (!sr.EndOfStream)
                    lista.Add(sr.ReadLine());

                sr.Close();
                tabAlumnos = new string[lista.Count];
                tabNotas = new float[lista.Count, 3];
                tabID = new int[lista.Count];

                for(int k=0; k<lista.Count; k++)
                {
                    tabID[k] = Convert.ToInt32(lista[k].Substring(0, 3));
                    tabAlumnos[k] = lista[k].Substring(3, 28);
                    tabNotas[k, 0] = (float)Convert.ToDouble(lista[k].Substring(31, 3));
                    tabNotas[k, 1] = (float)Convert.ToDouble(lista[k].Substring(34, 3));
                    tabNotas[k, 2] = (float)Convert.ToDouble(lista[k].Substring(37, 3));

                }


            }


        }
    }
}
