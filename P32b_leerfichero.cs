﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text;


namespace P32x_Leer
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto, largo="";
            int parrafos=0;
            int tam = 0;
            Console.Write("nombre del fichero: ");
            string fichero = Console.ReadLine();
            if (!File.Exists(@"C:/zDatosPruebas/" + fichero+".txt"))
                Console.WriteLine("ERROR [fichero no encontrado]");
            else
            {
                StreamReader lector = new StreamReader(@"C:/zDatosPruebas/" + fichero + ".txt");

                do
                {
                    texto = lector.ReadLine();
                    parrafos++;

                    if (!(texto == null))
                        if (texto.Length > tam)
                        {
                            tam = texto.Length;
                            largo = texto;
                        }


                } while (!(texto == null));
                lector.Close();
                Console.WriteLine("Hay un total de {0} parrafos, el más largo, con {1} caracteres dice: \n {2}", parrafos, tam, largo);

            }
            Console.WriteLine("Saliendo...");
            Console.ReadKey(true);


        }
    }
}
