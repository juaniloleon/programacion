﻿/* P3  25c)	Tabla2dNotas
Construye dos tablas tApell y tNomb como hiciste en la práctica anterior. 
 * A continuación realizamos el siguiente proceso:
1)	Construimos una tabla de string tAlumnos y la cargamos con los “Apellidos, Nombre” de cada alumno a partir de las 
    dos tablas anteriores.
2)	Construimos una tabla de floats tNotas con las mismas filas que la anterior pero de dos dimensiones 
    (tres columnas), para guardar las notas de los alumnos, es decir, en la fila n se guardarán las tres notas del alumno 
    de posición n. Esta tabla se cargará con notas obtenidas al azar, entre 0.0 y 9.9, (con un decimal).
3)	Presentamos la tabla de doble entrada, alumnos/notas donde aparece cada alumno con sus tres notas y la media de las tres. 
    A esta presentación se le pondrá una cabecera: 
                                    id  Alumno  Prog   ED    BD   Media


    Además se guarda en un fichero de texto
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        string[] tApellidos = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
        string[] tNombres = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Angel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafael", "Antonio" };
        string[] tAlumnos = ConcatenaTabla(tNombres, tApellidos);

        float[,] tNotas = new float[tAlumnos.Length, 3];
        tNotas = IniciaTabla(tNotas);
        int[] idR = TabRand(tAlumnos.Length);

        ImprimeteNotas(tAlumnos, tNotas, idR);

        GuardaSecuencial(tAlumnos, tNotas, idR);
        GuardaTamanyo(tAlumnos, tNotas, idR);


        Console.ReadKey(true);

    }

    private static void GuardaTamanyo(string[] tAlumnos, float[,] tNotas, int[] idR)
    {
        Console.Write("nombre fichero: ");
        string nombre = Console.ReadLine();

        StreamWriter sw = File.CreateText(nombre + ".txt");

        for(int i=0; i<tAlumnos.Length; i++)
        {
            sw.Write(idR[i] + ";" + tAlumnos[i]);
            for (int j = 0; j < tNotas.GetLength(1); j++)
                sw.Write(";" + tNotas[i, j]);
            sw.WriteLine();
        }
        sw.Close();


    }

    private static void GuardaSecuencial(string[] tAlumnos, float[,] tNotas, int[] idR)
    {
        Console.Write("nombre fichero (distinto): ");
        string nombre = Console.ReadLine();

        StreamWriter sw = File.CreateText(nombre + ".txt");

        for (int i = 0; i < tAlumnos.Length; i++)
        {
            sw.Write(idR[i] + ";" + tAlumnos[i]);
            for (int j = 0; j < tNotas.GetLength(1); j++)
                sw.Write(";" + tNotas[i, j]);
            sw.WriteLine();
        }
        sw.Close();
    }

    private static int[] TabRand(int length)
    {
        Random rnd = new Random();
        int[] tabla = new int[length];
        List<int> lista = new List<int>();
        int aux;

        for (int i = 10; i < 100; i++)
            lista.Add(i);

        for (int i = 0; i < length; i++)
        {
            aux = rnd.Next(lista.Count);
            tabla[i] = lista[aux];
            lista.Remove(aux);
        }
        return tabla;
    }

    private static void ImprimeteNotas(string[] tAlumnos, float[,] tNotas, int[] idR)
    {
        float media;

        Console.SetCursorPosition(3, 1);

        Console.WriteLine("id\tAlumno\t\t\t\tProg\tED\tBD\tMedia");

        for (int i = 0; i < tAlumnos.GetLength(0); i++)
{
            Console.SetCursorPosition(3, (2 + i));
        
            media = 0;
            Console.Write("{0})\t{1}", idR[i], tAlumnos[i]);

            for (int j = 0; j < tNotas.GetLength(1); j++)
            {
                Console.SetCursorPosition(40 + (8 * j), (2 + i));
                Console.Write("{0}\t", tNotas[i, j]);
                media += tNotas[i, j];
            }
            Console.WriteLine(Math.Round(media / 3, 2));
        }


    }

    private static float[,] IniciaTabla(float[,] tNotas)
    {
        Random rnd = new Random();
        for (int i = 0; i < tNotas.GetLength(0); i++)
            for (int j = 0; j < tNotas.GetLength(1); j++)
                tNotas[i, j] = ((float)rnd.Next(0, 100) / 10);

        return tNotas;
    }

    private static string[] ConcatenaTabla(string[] tNombres, string[] tApellidos)
    {
        string[] tabNomAp = new string[tNombres.Length];

        for (int i = 0; i < tabNomAp.Length; i++)
            tabNomAp[i] = (tApellidos[i] + ", " + tNombres[i]);


        return tabNomAp;
    }
    private static void ImprimeteTabla2D(string[,] tab2dGente)
    {

        for (int i = 0; i < tab2dGente.GetLength(0); i++)
        {
            Console.Write("\t {0}) ", i);
            for (int j = 0; j < tab2dGente.GetLength(1); j++)
            {
                Console.Write(tab2dGente[i, j] + " ");
            }
            Console.WriteLine();
        }
    }

    private static void ImprimeteTabla2D(double[,] tab2dGente)
    {

        for (int i = 0; i < tab2dGente.GetLength(0); i++)
        {
            Console.Write("\t {0}) ", i);
            for (int j = 0; j < tab2dGente.GetLength(1); j++)
            {
                Console.Write(tab2dGente[i, j] + " ");
            }
            Console.WriteLine();
        }
    }
    private static void ImprimeteTabla(string[] tabla)
    {

        for (int i = 0; i < tabla.GetLength(0); i++)
            Console.WriteLine("\t {0}) {1}", i, tabla[i]);

    }

    private static string[,] CombinaTabla(string[] tNombres, string[] tApellidos)
    {
        string[,] tab2dGente = new string[tNombres.Length, 2];

        for (int i = 0; i < tNombres.Length; i++)
        {
            tab2dGente[i, 0] = tNombres[i];
            tab2dGente[i, 1] = tApellidos[i];

        }

        return tab2dGente;
    }



}