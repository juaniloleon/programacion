﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuestraDivisores
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = Convert.ToInt32(Console.ReadLine());

            List<int> lista = DivisoresDe(num);

            Console.ReadKey(true);

        }

        private static List<int> DivisoresDe(int num)
        {
            List<int> lista = new List<int>();

            for (int i = 2; i <= num / i; i++)
                if (num % i == 0)
                {
                    lista.Add(i);
                    if(i!=num/i)
                    lista.Add(num / i);
                }

            lista.Sort();
            for (int i = 0; i < lista.Count; i++)
                Console.WriteLine(lista[i]);

            return lista;
        }
    }
}
