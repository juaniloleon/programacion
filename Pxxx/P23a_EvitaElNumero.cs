﻿/*
Alumno: Leon Oyola, Juan Antonio

Se muestran en pantalla los números del 0 al 99, en 10 filas por 10
columnas, y Luego la máquina elige un valor al azar entre estos dos
A CONTinuación se pide al usuario que acierte el número oculto. Si
ACIERTA, pierde. Si se pasa, se borrarán todos los números desde el
final hasta el introducido. Si se queda corto, se borrarán todos los
números desde el principio hasta el introducido.
El siguiente jugador, tendrá que introducir un número de los que
queden en la pantalla.
Ejemplo: si el valor oculto es 50 y el disparo es 80, se borrarán todos
los valores entre 99 y 80 y el nuevo rango de valores posible será
entre 0 y 79. Si luego dispara 30, se borrarán desde el 0 al 30. Si
luego dispara 45, se borran del 31 al 45, etc.
Tienes que utilizar los métodos siguientes:
CapturaEntero, para introducir los números.
CompruebaTiro, que recibe el número que ha puesto el usuario,
responde si ha acertado o borra el rango adecuado, y devuelve un
booleano: true si ha acertado o false si no.


*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    public static int ACIERTA = 0;
    public static int CONT = 0;
    public static int MIN = 0;
    public static int MAX = 99;
    public static bool SONIDO = false;


    private static void ColocaElNumero(int num, bool color, bool SONIDO)
    {
        Random rnd = new Random();

        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Black;
        }

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;

        Console.SetCursorPosition(columna, fila);
        Console.Write(num);
        if (SONIDO == true)
            Console.Beep(rnd.Next(150, 1785), 100);

        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

        }
    }

    private static int CapturaEntero(string cadena, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write(cadena, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);

            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
            {

                Console.Write("** Error: Valor introducido no válido **");
            }
            else if (resultado < min || resultado > max)
            {

                Console.Write("** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    private static bool CompruebaTiro(int num)
    {
        bool correcto = false;
        Console.SetCursorPosition(4, 21);
        if (num == ACIERTA)
        {

            Console.Clear();
            Console.SetCursorPosition(4, 5);

            Console.WriteLine("\t Has perdido en {0} tiros", CONT);
            correcto = true;
        }
        else if (num < ACIERTA)
        {
            Console.Write("Te has quedado corto");
            for (int i = MIN; i <= num; i++)
                ColocaElNumero(i, true, SONIDO);
            MIN = num;

        }
        else
        {
            Console.Write("Te has pasado\t\t");
            for (int i = MAX; i >= num; i--)
                ColocaElNumero(i, true, SONIDO);
            MAX = num;
        }

        return correcto;
    }

    
    static void Main(string[] args)
    {

        ConSonido();


        Random rnd = new Random();
        
        ACIERTA = rnd.Next(MIN, MAX + 1);
        Console.WriteLine("el numero es " + ACIERTA);
        int num;

        for (int i = 0; i < 100; i++)
            ColocaElNumero(i, false, SONIDO);

        do
        {
            Console.SetCursorPosition(11, 22);
            Console.WriteLine("\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t");
            Console.SetCursorPosition(11, 22);

            num = CapturaEntero("\t" + (CONT + 1) + ".- Prueba suerte: ", MIN, MAX);
            CONT++;
            
        } while (!CompruebaTiro(num));

        Console.ReadKey();
    }

    private static void ConSonido()
    {
        Console.WriteLine("Presione 'S' para activar el sonido");
        char s = Console.ReadKey().KeyChar;
        if (s == 'S' || s == 's')
            SONIDO = true;

        Console.Clear();
    }
}

