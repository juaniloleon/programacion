﻿// Alumno: Cadenas Personal, Guillermo 

/* P

*/

using System;
using System.Collections.Generic;
using System.Text;

class Program
{
   static string[] tApellidos = { "Sanchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vazquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
    static string[] tNombres = { "Álavro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Ángel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafaél", "Antonio" };


    private static void Pausa(string frase)
    {

        Console.Write("\n\nPulse una tecla para "+frase+":");
        Console.ReadKey(true);

    }

    static void Main(string[] args)
    {
       List<string> listaApellNomb = new List<string>();
        
        DevuelveListaGente(tApellidos,tNombres,listaApellNomb);

        Pausa("mostrar la primera lista de gente:");
        MuestraColeccion(listaApellNomb,0);

        int a = CapturaEntero("Posición a cambiar:",0,43);
        int b = CapturaEntero("¿Por cuál?", 0, 43);

        IntercambiaPos(listaApellNomb,a,b);

        MuestraColeccion(listaApellNomb,40);

        Pausa("limpiar la pantalla (tranquilo, no usare ningún liquido)");
        Console.Clear();

        MuestraColeccion(listaApellNomb, 0);

        int aEliminar = CapturaEntero("Que nº quiere eliminar?",0,43);

        BorraElemento(listaApellNomb,aEliminar);

        MuestraColeccion(listaApellNomb,40);

        Pausa("quitarle el polvo a la pantalla");
        Console.Clear();

        MuestraColeccion(listaApellNomb, 0);

        string aEliminar2 = CapturaEntero2(listaApellNomb,"escriba el nombre que quiera borrar: ");
        BorraElemento2(listaApellNomb, aEliminar2);

        MuestraColeccion(listaApellNomb, 40);

        Console.Write("\n\n\n\t\tPulsa una tecla para salir");
        Console.ReadKey(true);
    }

     static void BorraElemento2(List<string> listaApellNomb, string aEliminar2)
    {
        listaApellNomb.Remove(aEliminar2);
    }

    static string CapturaEntero2(List<string> lista, string texto)
    {

        string a;
       
            Console.Write(texto);
            a = Console.ReadLine();

           bool b = lista.Contains(a);

            while (!b)
            {
                Console.Write("El nombre que ha introducido no existe..");
                Console.Write(texto);
                a = Console.ReadLine();
            b = lista.Contains(a);
        }

           
 return a;
    }
       
    

    static void BorraElemento(List<string> lista, int a)
    {
    
        lista.RemoveAt(a);
    }

    static void IntercambiaPos(List<string> lista, int a, int b)
    {
        string c;
        c = lista[a];
        lista[a] = lista[b];
        lista[b] = c;
        
    }

    static void MuestraColeccion(List<string> lista, int columna)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(columna,5+i);
            Console.WriteLine("{0}) {1}",i,lista[i]);
        }
    }

    static void DevuelveListaGente(string[] tApellidos, string[] tNombres,List<string> lista)
    {
       
   
        for (int i = 0; i < tNombres.Length; i++)
        {
            lista.Add(tApellidos[i]);
            lista.Add(tNombres[i]);
        }
    }

    static int CapturaEntero(string texto, int min, int max)
    {
        int num;
        bool esCorrecto;
        do
        {
            Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
            if (!esCorrecto)
                Console.WriteLine("\n\t ** ERROR de FORMATO **");
            else if (num < min || num > max)
            {
                Console.WriteLine("\n\t ** ERROR: VALOR FUERA DE RANGO **");
                esCorrecto = false;
            }
        } while (!esCorrecto);

        return num;
    }

}