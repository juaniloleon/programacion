﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    private static void ColocaElNumero(int num, bool color)
    {
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;

        Console.SetCursorPosition(columna, fila);
        Console.Write(num);
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }

    private static int CapturaEntero(string cadena, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write(cadena, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);

            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
            {
               
                Console.Write("** Error: Valor introducido no válido **");
            }
            else if (resultado < min || resultado > max)
            {
             
                Console.Write("** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    private static bool CompruebaTiro(int num)
    {
        bool correcto = false;
        Console.SetCursorPosition(4, 21);
        if (num == acierta)
        {

            Console.Clear();
            Console.SetCursorPosition(4, 5);

            Console.WriteLine("\t Has acertado en {0} intentos", cont);
            correcto = true;
        }
        else if (num < acierta)
            Console.Write("Te has quedado corto");
        else
            Console.Write("Te has pasado\t\t");


        return correcto;
    }

   public static int acierta = 0;
   public static int cont = 0;

    static void Main(string[] args)
    {
        Random rnd = new Random();

        int min = CapturaEntero("\tIntroduzca un mínimo [{0},{1}]: ",1, 10);
        int max = CapturaEntero("\tIntroduzca un máximo [{0},{1}]:", min, 100);
        Console.Clear();
        acierta = rnd.Next(min, max+1);
        Console.WriteLine("el numero es " + acierta);
        int num;

        for (int i = 0; i < 100; i++)
            ColocaElNumero(i, false);

        do
        {
            Console.SetCursorPosition(11, 22);
            Console.WriteLine("\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t\t\t\t\t");
            Console.SetCursorPosition(11, 22);

            num = CapturaEntero("\t"+(cont+1)+".- Prueba suerte: ", 0, 99);
            cont++;

            ColocaElNumero(num, true);

        } while (!CompruebaTiro(num));

        Console.ReadKey();
    }

    

}

