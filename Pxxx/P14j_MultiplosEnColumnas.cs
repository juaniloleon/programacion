﻿/*
14j. MultiplosGlobalEnColumnas: 			     (45min)
Se pide un número cant, entre 100 y 300, 
un número inicial ni,  entre 1000 y 2000, 
un número mul, entre 11 y 77 
y un número nc, entre 3 y 9. 
El programa presentará los cant múltiplos de mul, a partir de ni en nc columnas. 
Ejemplo: si cant= 200, ni = 1500, mul=30 y nc =6, 
el programa presentará los 200 primeros múltiplos de 30 a partir del 1500 en 6 columnas.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        int cant, ni, mul, nc;
        bool esCorrecto;
        // Captura del número de múltiplos a presentar
        do
        {
            Console.Write("\n\tDime la cantidad de múltiplos a presentar [100..300]: ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out cant);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (cant < 100 || cant > 300)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        // Captura del valor inicial ni
        do
        {
            Console.Write("\n\tDime el valor inicial [1000..2000]: ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out ni);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (ni < 1000 || ni > 2000)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }

        } while (esCorrecto == false);

        // Captura del número del que vamos a obtener sus múltiplos  mul
        do
        {
            Console.Write("\n\tDime el número del que vamos a obtener sus múltiplos [11..77]: ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out mul);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (mul < 11 || mul > 77)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }

        } while (esCorrecto == false);

        // Captura del número de columnas en las que lo voy a presentar  nc
        do
        {
            Console.Write("\n\tDime el número de columnas [3..9]: ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out nc);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (nc < 3 || nc > 9)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }

        } while (esCorrecto == false);

        // cálculo del primer múltiplo
        int multiplo = (ni / mul) * mul;
        if (multiplo < ni)
            multiplo += mul;

        // presentación
        for (int i = 0; i < cant; i++)
        {
            // cuando el contador sea múltiplo del nº de columnas...
            // Salto de línea
            if (i % nc == 0)
                Console.WriteLine();

            Console.Write("{0}\t", multiplo);
            multiplo += mul;
        }



        Console.Write("\n\nPulsa intro para salir");
        Console.ReadLine();
    }
}