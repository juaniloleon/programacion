﻿/*
Alumno: León Oyola, Juan Antonio
P27c_ListaDeGente

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.White;

        string[] apellidos = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
        string[] nombres = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Angel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafael", "Antonio" };
        // CargaAlumnos nos devolverá la tabla de alumnos que guardaremos en tAlumnos
        Pausa("cargar la lista");
        List<string> listaApellNomb = DevuelveListaGente(apellidos, nombres);

        Pausa("mostrar la lista");
        MuestraColeccion(listaApellNomb, 0);
        Pausa("continuar");


        int pos1 = CaputuraEntero("Pos1 a cambiar: ", 0, listaApellNomb.Count()-1);
        int pos2 = CaputuraEntero("Pos2 a cambiar: ", 0, listaApellNomb.Count()-1);

        Pausa("intercambiar posiciones");

        IntercambiaPos(listaApellNomb, pos1, pos2);
        Pausa("mostrar la tabla cambiada");
        MuestraColeccion(listaApellNomb, 40);

        Pausa("continuar");
        Console.Clear();
        MuestraColeccion(listaApellNomb, 0);

        int pose =CaputuraEntero("elemento a eliminar: " ,0, listaApellNomb.Count()-1);
        Pausa("eliminar el elemento:");
        if (BorraElemento(listaApellNomb, pose) == false)
            Pausa("continuar [no se pudo eliminar]");
        else
        {

            Pausa("mostrar la tabla cambiada");
            MuestraColeccion(listaApellNomb, 40);
        }


        Pausa("continuar");
        Console.Clear();
        MuestraColeccion(listaApellNomb, 0);


        string eliminar = CaputuraEntero("Apell, Nombre a suprimir: ");
        Pausa("eliminar el elemento:");
        if (BorraElemento(listaApellNomb, eliminar) == false)
            Pausa("continuar [no se pudo eliminar]");

        else
        {

            Pausa("mostrar la tabla cambiada");
            MuestraColeccion(listaApellNomb, 40);
        }


        Pausa("salir");
    }




    private static bool BorraElemento(List<string> lista, string element)
    {
        int id = lista.IndexOf(element);
        if (id==-1)
            return false;

        else lista.RemoveAt(id);
        return true;
    }

    private static bool BorraElemento(List<string> lista, int pos)
    {
        if (lista.Count() > pos && pos >= 0)
            lista.RemoveAt(pos);
        else return false;
        return true;
    }

    private static List<string> IntercambiaPos(List<string> lista, int pos1, int pos2)
    {
        string aux = lista[pos1];
        
        lista.Insert(pos1, lista[pos2]);
        lista.RemoveAt(pos1+1);
        lista.Insert(pos2, aux);
        lista.RemoveAt(pos2 + 1);

        return lista;
    }

    private static List<string> DevuelveListaGente(string[] apellidos, string[] nombres)
    {
        List<string> lista = new List<string>();

        for(int i=0; i<apellidos.Length; i++)
            lista.Add( apellidos[i]+", "+nombres[i]);
                
        return lista;
    }

    private static void Pausa(string texto)
    {
        Console.ForegroundColor = ConsoleColor.Red;
        BorraLinea();
        Console.Write("Pulsa una tecla para " + texto+"\t\t\t");
        Console.ForegroundColor = ConsoleColor.White;

        Console.ReadKey(true);


    }
    private static void MuestraColeccion(List<string> lista, int col)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(col, 5 + i);
            Console.WriteLine(i+") "+lista[i]);
        }

    }

    private static int CaputuraEntero(string texto, int min, int max)
    {
        Console.ForegroundColor = ConsoleColor.Magenta;

        bool esCorrecto;
        int resultado;
        do
        {
            BorraLinea();
            Console.Write("{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
            {
                BorraLinea();

                Pausa("continuar [valor no valido]");
               
            }
            else if (resultado < min || resultado > max)
            {
                BorraLinea();

                Pausa("continuar [valor fuera de rango]");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        Console.ForegroundColor = ConsoleColor.White;

        return resultado;
    }

    private static string CaputuraEntero(string texto)
    {
        Console.ForegroundColor = ConsoleColor.Magenta;

        BorraLinea();
        Console.Write("\t{0}: ", texto);
        Console.ForegroundColor = ConsoleColor.White;
        return Console.ReadLine();

    }

    private static void BorraLinea()
    {
        Console.SetCursorPosition(3, 2);
        Console.Write("\t\t\t\t\t\t\t\t\t\t\t");
        Console.SetCursorPosition(3, 2);

    }
}



