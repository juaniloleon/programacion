﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{

    private static void ColocaElNumero(int num)
    {

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;

        Console.SetCursorPosition(columna, fila);
        Console.Write(num);
   //   Console.Beep((150 - num) * 10, 100);



    }
    private static int CapturaEntero(int min, int max)
    {
        bool esCorrecto;
        int resultado;
        Console.ForegroundColor = ConsoleColor.White;
        do
        {
            Console.BackgroundColor = ConsoleColor.Black;

            Console.SetCursorPosition(10, 22);
            Console.Write("\t\t\t\t\t\t\t\t\t ");
            Console.BackgroundColor = ConsoleColor.Red;

            Console.SetCursorPosition(10, 22);

            Console.Write("Introduzca un numero faltante ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }


    static void Main(string[] args)
    {
        Random rnd = new Random();
        int num = 0;

        for (int i=0; i<200; i++)
        {

            ColocaElNumero(rnd.Next(0, 100));
        }

        do
        {
            num = CapturaEntero(0, 100);
            ColocaElNumero(num);


        } while (num != 0);
        Console.Clear();
        Console.ReadKey();
    }
}


