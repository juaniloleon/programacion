﻿/*
Alumno: Leon Oyola, Juan Antonio

P21b_MetodoPresentaImpares

Método que 
Recibe: dos enteros, min y max.
Hace: presenta los imapres entre min y max, sin incluirlos.
Devuelve: nada.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P21b_PresentaImpares
{
    class Program
    {
        static void PresentaImpares(int min, int max)
        {

            if (min % 2 != 0)
                min += 2;
            else min++;

            for (int i = min; i < max; i += 2)
                Console.Write(i + "\t");

        }
        private static int CapturaEntero()
        {
            bool esCorrecto;
            int resultado;
            do
            {
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
            } while (esCorrecto == false);

            return resultado;
        }
        static void Main(string[] args)
        {

            int min=CapturaEntero();
            int max = CapturaEntero();



        }

  
    }
}
