﻿/*

Alumno: Leon Oyola, Juan Antonio

27b
1) Carga una tabla de enteros cuyo tamaño se pide al usuario (10-90)
2) Carga una lista de enteros con los valores anteriores
3) Mostrar en una columna los valores de la tabla
4) Mostrar en pantalla a la derecha de la columna anterior los valores de la Lista
5) Ordenar la lista y mostrarla a la derecha
6) Invertir la lista y mostrar a la derecha

Utilizando GeneraTabla:
    Recibe el tamaño de la tabla y devuelve una tabla de dicho tamaño cargaa con aleatorios de 2 cifras
MuestraColeccion:
    Recibe tabla de enteros y int col, muestra los elementos de la tabla colocados en la columna col
MuestraColeccion:
    Recibe lista de enteros y int col, muestra los elementos de la lista colocados en la columna col
Pausa:
Recibe un texto y muestra el mensaje "Pulsa una tecla para" +texto. Esperará una pulsación para que continúe el programa

*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static Random rnd = new Random();
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.White;

        int tam = CaputuraEntero("Numero de elementos", 10, 99);
        Console.Clear();
        int[] tabla = GeneraTabla(tam);

        List<int> lista = rellenaLista(tabla);

        Pausa(" mostrar la tabla\t\t\t");
       // imprimeEn(" T", 0);
        MuestraColeccion(tabla, 0);
        
        Pausa(" mostrar la lista\t\t\t");
        //imprimeEn(" L", 1);
        MuestraColeccion(lista, 1);

        Pausa(" mostrar la lista ordenada\t\t\t");
        //imprimeEn(" S", 2);
        lista.Sort();
        MuestraColeccion(lista, 2);

        Pausa(" mostrar la lista inversa\t\t\t");
        //imprimeEn(" I", 3);
        lista.Reverse();
        MuestraColeccion(lista, 3);

        Pausa(" salir\t\t\t");

    }

    private static void Pausa(string texto)
    {
        Console.SetCursorPosition(3, 2);
        Console.Write("Pulsa una tecla para" + texto);
        Console.ReadKey(true);

    }
    private static void MuestraColeccion(List<int> lista, int col)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(3 + (col * 7), 5 + i);
            Console.WriteLine(lista[i]);
        }

    }
    private static void MuestraColeccion(int[] tabla, int col)
    {
        Console.WriteLine();
        for (int i = 0; i < tabla.Length; i++)
        {
            Console.SetCursorPosition(3 + (col * 7), 5 + i);

            Console.WriteLine(tabla[i]);
        }
    }
    private static int[] GeneraTabla(int tam)
    {
        int[] tabla = new int[tam];

        for (int i = 0; i < tam; i++)
            tabla[i] = rnd.Next(10, 100);


        return tabla;
    }
    private static int CaputuraEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

    private static List<int> rellenaLista(int[] tabla)
    {
        List<int> lista = new List<int>();

        for (int i = 0; i < tabla.Length; i++)
            lista.Add(tabla[i]);

        return lista;
    }
    private static void imprimeEn(string texto, int columna)
    {
        Console.SetCursorPosition(3 + columna * 7, 3);
        Console.BackgroundColor = ConsoleColor.Red;
        Console.ForegroundColor = ConsoleColor.Black;

        Console.Write(texto);
        Console.ForegroundColor = ConsoleColor.White;

        Console.BackgroundColor = ConsoleColor.Black;

    }
}



