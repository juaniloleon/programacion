﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        int filas = 10;// CapturaEntero("Numero de filas", 1, 100);
        int columnas = 7;// CapturaEntero("Numero de columnas", 1, 100);
        double[,] tabla = new double[filas, columnas];
        bool modificar = false;

        tabla = RellenaTabla(tabla);
        ImprimeTabla(tabla);

        do
        {
            Console.Write("¿Quiere modificar algún valor? (y/n): ");
            if (Console.ReadKey().KeyChar == 'y' || Console.ReadKey().KeyChar == 's')
            {
                modificar = true;
                EditaTabla(tabla, CapturaEntero("Fila: ", 0, tabla.GetLength(0)), CapturaEntero("Columna: ", 0, tabla.GetLength(1)));

            }
            else if (Console.ReadKey().KeyChar == 'n')
                modificar = false;

            else modificar = true;

        } while (modificar);

        Console.Write("Saliendo...");
        Console.ReadKey(true);

    }

    private static double[,] RellenaTabla(double[,] tabla)
    {
        Random rnd = new Random();
        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                // tabla[i, j] = CapturaEntero("Valor para [" + i + "," + j + "]: ", 0, 100);
                //tabla[i,j](rnd.Next(10000) / 100.00);
                tabla[i, j] = Math.Round((rnd.NextDouble() * 100), 2);

            }
        }
        return tabla;
    }
    private static double[,] EditaTabla(double[,] tabla, int fila, int columna)
    {
        tabla[fila, columna] = CapturaDouble("Nuevo valor: ", 0.00, 99.99);

        return tabla;
    }
    private static void ImprimeTabla(int[,] tabla2D)
    {
        Console.ReadKey();
        Console.WriteLine("\t0\t1\t2\t3\t4\t5\t6");
        for (int i = 0; i < tabla2D.GetLength(0); i++)
        {
            Console.Write(i);
            for (int j = 0; j < tabla2D.GetLength(1); j++)
            {
                Console.Write("\t" + tabla2D[i, j]);
            }
            Console.WriteLine("\n");

        }
    }
    private static void ImprimeTabla(double[,] tabla)
    {
        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                Console.Write("\t{0}", tabla[i, j].ToString("00.00"));
            }
            Console.WriteLine("\n");

        }
    }
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    private static double CapturaDouble(string texto, double min, double max)
    {
        bool esCorrecto;
        double resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Double.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
}

