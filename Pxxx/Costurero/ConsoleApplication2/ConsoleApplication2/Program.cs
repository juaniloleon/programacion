﻿/*
Alumno: Leon Oyola, Juan Antonio


P2xx_MenuMultiplosMetodos
Realiza un menú que tenga, además de la opción (0.- Salir) los cuatro
últimos apartados de la práctica anterior (14a):
1) Múltiplos Menores De300;
2) Trescientos Primeros Múltiplos;
3) Múltiplos Entre 500 y 700
4) Ochenta Múltiplos Desde 700.
En cada una se pide un número de dos cifras (verificándolo) y
resuelve la opción elegida por el método que te parezca mejor.
Después de ejecutar cada opción mostrará el texto
“Pulsa una tecla para volver al Menú”.
Sólo cuando se elija la opción 0 acabará el programa.
Hay que cumplir estos requisitos:
 Evitar las repeticiones innecesarias de código (como siempre).
 Se elige la opción sin pulsar Intro (ReadKey).*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        char entrada;

        do
        {
            Console.Clear();


            Console.WriteLine("\n\n\t\t╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("\t\t║           Menú             ║");
            Console.WriteLine("\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t║ 1- Multiplos menores 300   ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 2- 300 primeros multiplos  ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 3- Multiplos 500-700       ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 4- Ochenta multiplos 700   ║");
            Console.WriteLine("\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 0- Salir                   ║");
            Console.WriteLine("\t\t╚════════════════════════════╝");

            entrada = Console.ReadKey().KeyChar;
            Menu(entrada);

            switch (entrada)
            {
                case '0':
                    break;

                case '1':
                    Multiplos();

                    break;
                case '2':
                    Multiplos300();

                    break;
                case '3':
                    Multiplos500700();

                    break;
                case '4':

                    Multiplos700();

                    break;

            }

        } while (entrada != '0');


        Console.ReadLine();

    }

    private static void Multiplos700()
    {
        int num = CapturaEntero("Introduzca un numero", 0, 700);
        int primer = num * (700 / num);
        int i = 0;
        if (primer <= 700)
            primer += num;

        int aux = primer;

        while (i < 80)
        {
            Console.Write(aux + "\t");
            aux = aux + num;
            i++;

        }

        Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
        Console.ReadKey();
    }

    private static void Multiplos500700()
    {
        int num = CapturaEntero("Introduzca un valor", 0, 500);
        int primer = 0;

        if (500 % num == 0)
            primer = 500;
        else
            primer = 500 - (500 % num) + num;

        int aux = primer;


        while (aux <= 700)
        {
            Console.Write(aux + "\t");
            aux = aux + num;
        }

        Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
        Console.ReadKey();
    }

    private static void Multiplos300()
    {
        int num = CapturaEntero("Introduzca un valor: ", 0, 1000);
        int aux = num;
        int i = 0;
        while (i <= 300)
        {
            Console.Write(aux + "\t");
            aux = aux + num;
            i++;
        }

        Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
        Console.ReadKey();
    }

    private static void Menu(char entrada)
    {
        string enunciado;
        Console.Clear();

        switch (entrada)
        {
            case '0':
                enunciado = "         saliendo...        ";
                break;
            case '1':
                enunciado = " 1- Multiplos menores 300   ";
                break;
            case '2':
                enunciado = " 2- 300 primeros multiplos  ";
                break;
            case '3':
                enunciado = " 3- Multiplos 500-700       ";
                break;
            case '4':
                enunciado = " 4- Ochenta multiplos 700   ";
                break;
            default:
                enunciado = " ERROR: VALOR NO VALIDO  &5#";
                break;
        }

        Console.WriteLine("\n\t\t╔════════════════════════════╗");   //Impresion menu
        Console.WriteLine("\t\t║" + enunciado + "║");
        Console.WriteLine("\t\t╠════════════════════════════╝");
        Console.WriteLine("\t\t║");
        Console.WriteLine("\t\t║");
        Console.WriteLine("\t\t*");
    }

    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

    private static void Multiplos()
    {
        int num = CapturaEntero("Introduzca un numero:", 0, 300);


        Console.Write("\n Los numeros menores de 300 y multiplos de {0} son: \n \n", num);

        int i = num;
        while (i < 300)
        {
            Console.Write(i + "\t");
            i = i + num;

        }
        Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
        Console.ReadKey();
    }
}

