﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P21G_multiplicarSumando
{
    class Program
    {
        private static int CapturaEntero(string texto, int min, int max)
        {
            bool esCorrecto;
            int resultado;
            do
            {
                Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **");
                else if (resultado < min || resultado > max)
                {
                    Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            return resultado;
        }
        private static void Salida()
        {
            Console.Write("\n\n\t\t");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(" -> presione cualquier tecla para salir <- ");
            Console.ReadKey();

        }

        private static int MultiplicaSumando(int a, int b)
        {
            int total = 0;
            for (int i=0; i<a; i++)
            {

                total += b;
            }
            return total;
        }

        static void Main(string[] args)
        {

            int num1 = CapturaEntero("Introduzca un numero", 0, 10000);
            int num2 = CapturaEntero("Introduzca un numero", 0, 10000);
            int mult = MultiplicaSumando(num1, num2);
            Console.WriteLine("\n\tel producto es: " + mult );

            Salida();
        }

       
    }
}
