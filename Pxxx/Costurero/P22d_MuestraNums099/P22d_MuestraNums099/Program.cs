﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void MuestraNums099()
    {
        for (int i=0; i<100; i++)
        {
            Console.Write(i + "\t");
            if ((i+1) % 10 == 0)
                Console.WriteLine("\n");
        }

    }

    static void Main(string[] args)
    {
        Console.WriteLine("Pulse cualquier tecla para mostrar los numerosdel 0 al 99.\n\n");
        Console.ReadKey();
        Console.Clear();

        MuestraNums099();

        Console.WriteLine("Pulse cualquier tecla para salir");
        Console.ReadKey();
    }
}

