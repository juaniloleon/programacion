﻿/*
14j. MultiplosGlobalEnColumnas: 			     (45min)
Se pide un número cant, entre 100 y 300, 
un número inicial ni,  entre 1000 y 2000, 
un número mul, entre 11 y 77 
y un número nc, entre 3 y 9. 
El programa presentará los cant múltiplos de mul, a partir de ni en nc columnas. 
Ejemplo: si cant= 200, ni = 1500, mul=30 y nc =6, 
el programa presentará los 200 primeros múltiplos de 30 a partir del 1500 en 6 columnas.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

    static void Presentacion(int miCant, int ni, int mul, int nc)
    {
        // cálculo del primer múltiplo
        int multiplo = (ni / mul) * mul;
        if (multiplo < ni)
            multiplo += mul;

        // presentación
        for (int i = 0; i < miCant; i++)
        {
            // cuando el contador sea múltiplo del nº de columnas...
            // Salto de línea
            if (i % nc == 0)
                Console.WriteLine();

            Console.Write("{0}\t", multiplo);
            multiplo += mul;
        }

    }

    static void Main(string[] args)
    {
        int cant, ni, mul, nc;

        cant = CapturaEntero("Dime el número de múltiplos a presentar ", 100, 300);

        ni = CapturaEntero("Dime el Dime el valor inicial ", 1000, 2000);

        mul = CapturaEntero("Dime el número del que vamos a obtener sus múltiplos", 11, 77);

        nc = CapturaEntero("Dime el número de columnas", 3, 9);

        Presentacion(cant, ni, mul, 7);

        Console.Write("\n\nPulsa intro para salir");
        Console.ReadLine();
    }
}