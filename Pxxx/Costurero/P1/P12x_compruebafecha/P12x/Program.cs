﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12x
{
    class Program
    {
        static void Main(string[] args)
        {
            int mesMax = 0;
            Console.Write("Introduzca el año: ");
            int anyo = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduzca el mes: ");
            int mes = Convert.ToInt32(Console.ReadLine());


            if (mes > 0 && mes < 13)
            {
                Console.Write("Introduzca el dia: ");
                int dia = Convert.ToInt32(Console.ReadLine());
                switch (mes)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        mesMax = 31;
                        break;
                    case 2:
                        if ((anyo % 4 == 0 && anyo % 100 != 0) || (anyo % 4 == 0 && anyo % 400 == 0 && anyo % 100 == 0)) { mesMax = 29; }
                        else mesMax = 28;
                        break;
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        mesMax = 30;
                        break;

                }

                if (dia > 0 && dia <= mesMax)
                {
                    Console.Write("La fecha es:  {0} de ", dia);

                    switch (mes)
                    {
                        case 1:
                            Console.Write("enero");
                            break;
                        case 2:
                            Console.Write("febrero");
                            break;
                        case 3:
                            Console.Write("marzo");
                            break;
                        case 4:
                            Console.Write("abril");
                            break;
                        case 5:
                            Console.Write("mayo");
                            break;
                        case 6:
                            Console.Write("junio");
                            break;
                        case 7:
                            Console.Write("julio");
                            break;
                        case 8:
                            Console.Write("agosto");
                            break;
                        case 9:
                            Console.Write("septiembre");
                            break;
                        case 10:
                            Console.Write("octubre");
                            break;
                        case 11:
                            Console.Write("noviembre");
                            break;
                        case 12:
                            Console.Write("diciembre");
                            break;
                    }
                }
                Console.WriteLine(" del {0}.", anyo);

            }

            else Console.WriteLine("Mes no valido");

            Console.WriteLine("Pulse intro para salir:");
            Console.ReadLine();
        }


    }
}