﻿/*
Alumno: Leon Oyola, Juan Antonio


P14b_MenuMultiplos
Realiza un menú que tenga, además de la opción (0.- Salir) los cuatro
últimos apartados de la práctica anterior (14a):
1) Múltiplos Menores De300;
2) Trescientos Primeros Múltiplos;
3) Múltiplos Entre 500 y 700
4) Ochenta Múltiplos Desde 700.
En cada una se pide un número de dos cifras (verificándolo) y
resuelve la opción elegida por el método que te parezca mejor.
Después de ejecutar cada opción mostrará el texto
“Pulsa una tecla para volver al Menú”.
Sólo cuando se elija la opción 0 acabará el programa.
Hay que cumplir estos requisitos:
 Evitar las repeticiones innecesarias de código (como siempre).
 Se elige la opción sin pulsar Intro (ReadKey).*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        char entrada;
        int i, num, aux, primer;
        bool correcto = false;
        string enunciado = "";


        do
        {
            Console.Clear();


            Console.WriteLine("\n\n\t\t╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("\t\t║           Menú             ║");
            Console.WriteLine("\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t║ 1- Multiplos menores 300   ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 2- 300 primeros multiplos  ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 3- Multiplos 500-700       ║");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 4- Ochenta multiplos 700   ║");
            Console.WriteLine("\t\t╠════════════════════════════╣");
            Console.WriteLine("\t\t║                            ║");
            Console.WriteLine("\t\t║ 0- Salir                   ║");
            Console.WriteLine("\t\t╚════════════════════════════╝");


            entrada = Console.ReadKey().KeyChar;

            correcto = false;  //reset variables;
            i = 0;
            num = 0;
            aux = 0;
            primer = 0;


            Console.Clear();

            switch (entrada)
            {
                case '0':
                    enunciado = "         saliendo...        ";
                    break;
                case '1':
                    enunciado = " 1- Multiplos menores 300   ";
                    break;
                case '2':
                    enunciado = " 2- 300 primeros multiplos  ";
                    break;
                case '3':
                    enunciado = " 3- Multiplos 500-700       ";
                    break;
                case '4':
                    enunciado = " 4- Ochenta multiplos 700   ";
                    break;
                //default:
                //    enunciado = " ERROR: VALOR NO VALIDO  &5#";
                //    break;
            }

            Console.WriteLine("\n\t\t╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("\t\t║" + enunciado + "║");
            Console.WriteLine("\t\t╠════════════════════════════╝");
            Console.WriteLine("\t\t║");
            Console.WriteLine("\t\t║");


            if (entrada >= '1' && entrada <= '4')
            {
                Console.Write("\t\t╚> Introduzca numero de dos cifras: ");
                num = Convert.ToInt32(Console.ReadLine());
            }
            else
                Console.WriteLine("\t\t╚> [INTRO]");
                      

            switch (entrada)
            {
                case '0':
                    break;

                case '1':
                   
                    do
                    {
                        if (num > 9 && num < 100)
                            correcto = true;
                        else
                        {
                            Console.Write("\n ERROR el numero debe ser de dos cifras: ");
                            num = Convert.ToInt32(Console.ReadLine());
                        }


                    } while (correcto != true);


                    Console.Write("\n Los numeros menores de 300 y multiplos de {0} son: \n \n", num);

                    i = num;
                    while (i < 300)
                    {
                        Console.Write(i + "\t");
                        i = i + num;

                    }
                    Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
                    Console.ReadKey();


                    break;
                case '2':
                   do
                    {
                        if (num > 9 && num < 100)
                            correcto = true;
                        else
                        {
                            Console.Write("\nERROR el numero debe ser de dos cifras: ");
                            num = Convert.ToInt32(Console.ReadLine());
                        }


                    } while (correcto != true);
                    aux = num;

                    while (i <= 300)
                    {
                        Console.Write(aux + "\t");
                        aux = aux + num;
                        i++;
                    }

                    Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
                    Console.ReadKey();

                    break;
                case '3':

                    do
                    {
                        if (num > 9 && num < 100)
                            correcto = true;
                        else
                        {
                            Console.Write("\nERROR el numero debe ser de dos cifras: ");
                            num = Convert.ToInt32(Console.ReadLine());
                        }

                    } while (correcto != true);


                    if (500 % num == 0)
                        primer = 500;
                    else
                        primer = 500 - (500 % num) + num;

                    aux = primer;


                    while (aux <= 700)
                    {
                        Console.Write(aux + "\t");
                        aux = aux + num;
                    }

                    Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
                    Console.ReadKey();

                    break;
                case '4':
                    do
                    {
                        if (num > 9 && num < 100)
                            correcto = true;
                        else
                        {
                            Console.Write("\nERROR el numero debe ser de dos cifras: ");
                            num = Convert.ToInt32(Console.ReadLine());
                        }

                    } while (correcto != true);

                    primer = num * (700 / num);
                    if (primer <= 700)
                        primer += num;

                    aux = primer;

                    while (i < 80)
                    {
                        Console.Write(aux + "\t");
                        aux = aux + num;
                        i++;

                    }

                    Console.Write("\n\n\tPulsa una tecla para volver al Menú:  ");
                    Console.ReadKey();

                    break;

            }

        } while (entrada != '0');


        Console.ReadLine();

    }
}

