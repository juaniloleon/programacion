﻿/*
alumno: Leon Oyola, Juan Antonio

    Tabla de caracteres ASCII del 32 al 255 en 7 columnas


    formato:  67) C  68) D
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
              
        for (int i=32; i<256; i++)
        {
            if ((i + 3) % 7 == 0)
                Console.Write("\n");
            Console.Write("{0}) {1} \t", i, (char)i);
     


        }

        Console.ReadLine();
    }
}

