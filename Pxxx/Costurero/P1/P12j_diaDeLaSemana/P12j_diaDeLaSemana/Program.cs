﻿/*

Alumno: Leon Oyola, Juan Antonio



P21j_DiaDeLaSemana

Se le pide al usuario que introduzca un numero entro 0 y 6, cada uno los cuales
indica el día de la semana en que nos encontramos [0->lunes | 6-> domingo]

Luego pregunta cuantos días quier eavanzar numDias y el programa calcula
de la forma más eficiente en qué día de la semana caerá dentro de numDias

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12j_diaDeLaSemana
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombreDiaI="";
            string nombreDiaF= "";

            Console.Write("Introduzca día de inicio \n 0-> Lunes \n 1-> Martes \n 2-> Miercoles \n 3-> Jueves \n 4-> Viernes \n 5-> Sabado \n 6-> Domingo\n Dia:  ");
            int diaInicio = Convert.ToInt32(Console.ReadLine());
            int suma = 0;
            
            if (diaInicio >= 0 && diaInicio < 7)
            {
                if (diaInicio == 0) nombreDiaI = "Lunes";
                else if (diaInicio == 1) nombreDiaI = "Martes";
                else if (diaInicio == 2) nombreDiaI = "Miercoles";
                else if (diaInicio == 3) nombreDiaI = "Jueves";
                else if (diaInicio == 4) nombreDiaI = "Viernes";
                else if (diaInicio == 5) nombreDiaI = "Sabado";
                else  nombreDiaI = "Domingo";

                Console.Write("Introduca cuantos días quiere avanzar: ");
                int numDias = Convert.ToInt32(Console.ReadLine());

                suma = (numDias+diaInicio) % 7;
                
                if (suma == 0) nombreDiaF = "Lunes";
                else if (suma == 1) nombreDiaF = "Martes";
                else if (suma == 2) nombreDiaF = "Miercoles";
                else if (suma == 3) nombreDiaF = "Jueves";
                else if (suma == 4) nombreDiaF = "Viernes";
                else if (suma == 5) nombreDiaF = "Sabado";
                else nombreDiaF = "Domingo";
                
                Console.WriteLine("Si hoy es {0}, dentro de {1} dias será {2}.", nombreDiaI, numDias, nombreDiaF);
                
            }
            else Console.WriteLine("ERROR_1: Dia incorrecto.");




            Console.Write("Pulse intro para salir: ");
            Console.ReadLine();
        }
    }
}
