﻿/*
Alumno: Leon y Oyola, Juan Antonio


Se solicita un entero num de dos cifras (y verificado)

Se imprimen los multiplos de num menores de 300


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14a1_Presenta1
{
    class Program
    {
        static void Main(string[] args)
        {
            bool correcto = false;
            Console.Write("Introduzca numero de dos cifras: ");
            int num = Convert.ToInt32(Console.ReadLine());
            

            do
            {
                if (num > 9 && num < 100)
                    correcto = true;
                else
                {
                    Console.Write("ERROR el numero debe ser de dos cifras: ");
                    num = Convert.ToInt32(Console.ReadLine());
                }


            } while (correcto != true);


            Console.Write("Los numeros menores de 300 y multiplos de {0} son: \n ", num);

            int i = num;
            while (i < 300)
            {
                Console.Write(i + "\t");
                i = i + num;

            }

            Console.Write("\n \n \tAhora con do-while: [INTRO]  ");

            Console.ReadLine();

            i = num;
            do
            {
                Console.Write(i + "\t");
                i = i + num;

            } while (i < 300);

            Console.Write("\n \n \t Ahora con for: [INTRO]  ");

            Console.ReadLine();

            for (i = num; i < 300; i = i + num)
                Console.Write(i + "\t");
            

            Console.Write("\n\n\tIntro para salir:  ");

            Console.ReadLine();

        }

    }
}
