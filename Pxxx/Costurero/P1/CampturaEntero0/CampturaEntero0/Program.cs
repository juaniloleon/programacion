﻿// Alumno: Leon Oyola, Juan Antonio

/* P02e_CapturaEntero0
 Te pide unnumero entero de 2 cifras. si no cumpleelrequisito presentará un mensjae "numero fuera de rango"
 Si es corecto presenta "Correcto. El numero elegido es X*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        int numero;

        Console.Write("Introduzca un numero de dos cifras: ");
        numero = Convert.ToInt32(Console.ReadLine());

        if (numero >= 10 && numero < 100)
            Console.WriteLine("Correcto. El número elegido es {0}\n", numero);
        else
            Console.WriteLine("Número fuera de rango\n");


        Console.Write("Introduzca exit para salir. \n\n");
        Console.ReadLine();


    }
}

