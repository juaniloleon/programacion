﻿// Alumno: Leon Oyola, Juan Antonio

/* P02d_DesglosaEruros3
Se introduce la cantida de billetes de 20,5 y monedas de 2e
El programa averigua el total y lo desglosa en billetes de50,10 y monedas de 1
Las frases son coherentes y no imprime unidades a 0.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        int cantidad;       //cantidad a convertir
        int bil50, bil20, bil10, bil5, mon2, mon1;  //cantidad de billetes o monedas



        Console.Write("Introduzca la cantidad de billetes de 20: ");       //lectura 20
        bil20 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Introduzca el número de billetes de 5: ");       //lectura 5
        bil5 = Convert.ToInt32(Console.ReadLine());

        Console.Write("Introduzca el total de las monedas de 2: ");       //lectura 2
        mon2 = Convert.ToInt32(Console.ReadLine());

        cantidad = 20 * bil20 + 5 * bil5 + 2 * mon2;

        if (cantidad == 0)
        {
            Console.WriteLine("No ha introducido nada. no hay nada que desglosar. \n Esto es un programa serio, \npor favor introduzca valores si va a usar el programa. \n Ejecutar este programa sin darle el uso adecuado es estupido. \n Para hacer funcionar este ordenador \nse gastan recursos que podrían ser útiles a la comunidad.\n Usted es el único responsable de\nla perdida de tiempo.");

        }
        else
        {
            Console.Write("Ha introducido:");

            if (bil20 != 0)
            {
                if (bil20 == 1)
                    Console.Write(" 1 billete de 20");
                else Console.Write(" {0} billetes de 20", bil20);
                if (bil5 != 0)
                {
                    if (mon2 != 0)
                        Console.Write(",");
                    else
                        Console.Write(" y");
                }
            }
            if (bil5 != 0)
            {

                if (bil5 == 1)
                    Console.Write(" 1 billete de 5");
                else Console.Write(" {0} billetes de 5", bil5);
            }
            if (mon2 != 0)
            {
                if (bil20 != 0 || bil5 != 0)

                    Console.Write(" y");

                if (mon2 == 1)
                    Console.Write(" 1 moneda de 2");
                else Console.Write(" {0} monedas de 2", mon2);
            }

            Console.WriteLine(" que son " + cantidad + "e.");

            bil50 = cantidad / 50;                              //calculos
            bil10 = (cantidad - bil50 * 50) / 10;
            mon1 = (cantidad - bil50 * 50 - bil10 * 10);

            Console.Write(cantidad + "e son:");

            if (bil50 != 0)
            {
                if (bil50 == 1)
                    Console.Write(" 1 billete de 50");
                else Console.Write(" {0} billetes de 50", bil50);
                if (bil10 != 0)
                {
                    if (mon1 != 0)
                        Console.Write(",");
                    else
                        Console.Write(" y");
                }
            }
            if (bil10 != 0)
            {

                if (bil10 == 1)
                    Console.Write(" 1 billete de 10");
                else Console.Write(" {0} billetes de 10", bil10);
            }
            if (mon1 != 0)
            {
                if (bil10 != 0 || bil50 != 0)
                    Console.Write(" y");

                if (mon1 == 1)
                    Console.Write(" 1 moneda de 1");
                else Console.Write(" {0} monedas de 1", mon1);
            }
            Console.WriteLine(".");
        }




        Console.Write("\n\n\n\t\tPulsa Intro para salir");
        Console.ReadLine();
    }

}
