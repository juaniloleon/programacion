﻿/*
Alumno: Leon Oyola, Juan Antonio

Maquina elige numero la azar entre 10 y 20

El usurio tiene 3 oprtunidades par acertar.





*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p14e_AciertaNumero1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int num, lee;
            char q;

            do
            {
                num = rnd.Next(10, 20);
                lee = 0;
                
                Console.WriteLine("\t He pensado un numero del 10 al 19 (incluidos) \n");
                if (num%2==0)
                    Console.WriteLine("\t Es PAR \n");
                else
                    Console.WriteLine("\t Es IMPAR \n");
                if (num % 3 == 0)
                    Console.WriteLine("\t Es multiplo de 3 \n");
                else
                    Console.WriteLine("\t no es multiplo de 3 \n");


                for (int i = 1; i < 4; i++)
                {
                    if (lee != num)
                    {
                        do
                        {
                            Console.Write("\t" + i + " intento: ");
                            lee = Convert.ToInt32(Console.ReadLine());

                            if (lee < 10)
                                lee += 10;
                        }
                        while (lee < 10 || lee > 20);
                    }
                }

                Console.Write("\n\t");
                if (lee == num)
                    Console.WriteLine("CORRECTO, el numero era: " + num);
                else Console.WriteLine("OHHH! El numero era: " + num);

                Console.WriteLine("\n\t\t Para jugar de nuevo pulse cualquier tecla\n\t Para salir pulse Q ");
                q = Console.ReadKey().KeyChar;

                Console.Clear();

            } while (q != 'q');




        }
    }
}
