﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        int edad; //Declaración de 2 variables de tipo entero, edad y peso.

        string nombre;  //Declaración de variable de tipo cadena y nombre nombre.

        edad = 18; // asignación del valor
        edad = edad + 3;
        nombre = "Juanilo";

        Console.Write("Introduzca nombre: ");
        nombre = Console.ReadLine();

        Console.WriteLine("Hola " + nombre + ", tienes " + edad + " años de edad pero aparentas muchos menos. \n\n");


        Console.WriteLine("Seguramente no quieras, pero si quieres salir presiona intro.");
        Console.ReadLine();



    }
}
