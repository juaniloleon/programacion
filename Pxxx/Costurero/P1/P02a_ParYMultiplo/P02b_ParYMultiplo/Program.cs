﻿// Alumno: Leon Oyola, Juan Antonio

/* P02b_ParYmultiplo
Se introduce un numeo y el progrma comprueba si es par y multiplo de 3*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P02b_ParYMultiplo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\tIntroduzca el numero a comprobar: ");
            int num = Convert.ToInt32(Console.ReadLine());      //Lectura del valor


            if (num==0)
                Console.WriteLine("El cero NO es par ni impar");

            else if (num % 2 == 0)
            {
                if (num % 3 == 0)
                    Console.WriteLine("El numero {0} es PAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es PAR y no es múltiplo de 3", num);
            }
            else
            {
                if (num % 3 == 0)
                    Console.WriteLine("El numero {0} es IMPAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es IMPAR y no es múltiplo de 3", num);
            }




            /* //Versión con if en cascada
             bool par = false;           //Banderas de comprobacion
            bool mult = false;

            if (num % 2 == 0)       //rutina de comprobacion
                par = true;
            if (num % 3 == 0)
                mult = true;
                
                                //verion primera
            if (par == true && mult == true)
                Console.WriteLine("El numero {0} es PAR y además es múltiplo de 3", num);
           else  if (par == true && mult == false)
                Console.WriteLine("El numero {0} es PAR y no es múltiplo de 3", num);
            else if (par == false && mult == true)
                Console.WriteLine("El numero {0} es IMPAR y además es múltiplo de 3", num);
            else
                Console.WriteLine("El numero {0} es IMPAR y no es múltiplo de 3", num);

            /*
            if (par == true){
                if (mult == true)
                    Console.WriteLine("El numero {0} es PAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es PAR y no es múltiplo de 3", num);
            }
            else{
                if (mult == true)
                    Console.WriteLine("El numero {0} es IMPAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es IMPAR y no es múltiplo de 3", num);
            }*/
            
         

            Console.Write("\n\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }
    }
}
