﻿/*
 * Alumno: Leon Oyola, Juan Antonio
 * P01a Calcula la media
 * Septiembre 21, 2017
 * 
 ***/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01a_Calcula_la_media
{
    class Program
    {
        static void Main(string[] args)
        {
            float nota1, nota2, nota3, media;   //float para almacenamiento y calculos.
            string auxiliar; //String de caracter auxiliar para la entrada de datos.

            bool verdad = false;

            Console.WriteLine("\t\tBienvenido a P01a, su calculador de medias");

            Console.Write("\nIntroduzca primera calificación: ");     //Lectura 1
            auxiliar = Console.ReadLine();
            nota1 = Convert.ToInt32(auxiliar);

            Console.Write("\nIntroduzca segunda calificación: ");     //Lectura 2
            auxiliar = Console.ReadLine();
            nota2 = Convert.ToInt32(auxiliar);

            Console.Write("\nIntroduzca tercera calificación: ");     //Lectura 3
            auxiliar = Console.ReadLine();
            nota3 = Convert.ToInt32(auxiliar);

            Console.WriteLine("\nLas notas introducidas son {0}, {1}, {2}", nota1, nota2, nota3);

            media = (nota1 + nota2 + nota3) / 3;            //Operacion aritmetica
            Console.WriteLine("La media calculada es: "+ media);


            Console.WriteLine("\n\n\t\tGracias por usar nuestros servicios. Presione intro para salir");  //Cierre de la aplicacion
            Console.ReadLine();




        }
    }
}
