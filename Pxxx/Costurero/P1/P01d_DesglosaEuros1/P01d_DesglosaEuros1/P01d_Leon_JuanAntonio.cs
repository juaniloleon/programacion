﻿// Alumno: Leon Oyola, Juan Antonio

/* P01d 
Introduces una cantidad de euros y lo desglosa en billetes de 20, de 5 y monedas de 1euros.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01d_DesglosaEuros1
{
    class Program
    {
        static void Main(string[] args)
        {
            int cantidad;       //cantidad a convertir
            int bil20, bil5, mon1;  //cantidad de billetes o monedas

            Console.Write("Introduzca la cantidad a desglosar:");       //lectura

            cantidad = Convert.ToInt32(Console.ReadLine());

            bil20 = cantidad / 20;                              //calculos
            bil5 = (cantidad - bil20 * 20) / 5;
            mon1 = (cantidad - bil20 * 20 - bil5 * 5);
            
            Console.WriteLine("{0}e son {1} billetes de 20, {2} billetes de 5 y {3} monedas de 1", cantidad, bil20,bil5,mon1);        //salida de soluciones
             

            Console.Write("\n\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();

        }
    }
}
