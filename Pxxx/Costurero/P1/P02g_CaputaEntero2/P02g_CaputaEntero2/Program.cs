﻿// Alumno: Leon Oyola, Juan Antonio

/* P02g_CapturaEntero2
 Te pide unnumero comprendido entre dos valores. si no cumple el requisito presentará un mensjae "numero fuera de rango"
 Si es corecto presenta "Correcto. El numero elegido es X*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        int numero;
        int num1; //min
        int num2; //max

                                                                //solicitud rango
            Console.Write("Introduzca el valor minimo: ");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduzca el valor maximo: ");
            num2 = Convert.ToInt32(Console.ReadLine());


            Console.Clear();

            Console.Write("Introduzca un numero entre {0} y {1}: ", num1, num2);
            numero = Convert.ToInt32(Console.ReadLine());                       //solicitud numero

            if (num1 <= num2 && (numero >= num1 && numero <= num2))     //num1<num2 o igual
                Console.WriteLine("Correcto. El número elegido es {0}\n", numero);

            else if (num2 < num1 && (numero > num2 && numero < num1))       //num2<num1
                Console.WriteLine("Correcto. El número elegido es {0}\n", numero);
            else
                Console.WriteLine("Número fuera de rango\n");

        
        Console.Write("\n\nIntroduzca exit para salir. \n\n");
        Console.ReadLine();


    }
}

