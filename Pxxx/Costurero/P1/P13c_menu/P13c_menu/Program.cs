﻿/* alumno Leon Oyola, Juan Antonio


Menú0: 
Utilizando el “dibujo del menú” que se te entrega, realiza un programa que te presenta el Menú y 
te permite elegir una de sus opciones. Si se elige una opción correcta
—si no se dará el correspondiente mensaje de error— lo único que hará será limpiar 
la pantalla e imprimir en el centro de la misma: 
Ha elegido la opción nº …: <la que sea> 
Pulse Intro para Salir. 
En la opción 0 (Salir), sólo presenta este último mensaje 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P13c_menu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("║           Menú             ║");
            Console.WriteLine("╠════════════════════════════╣");
            Console.WriteLine("║     1) Opción Primera      ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     2) Segunda Opción      ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     3) Opción Tercera      ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     4) Cuarta Opción       ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     5) Listar Opciones     ║");
            Console.WriteLine("║____________________________║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     0) Salir               ║");
            Console.WriteLine("╚════════════════════════════╝");

            int opcion = Convert.ToInt32(Console.ReadLine());   //lecutura opcion

            Console.Clear();
            switch (opcion)                             //bloques 
            {
                case 1:
                    Console.WriteLine("\tHa elegido la opción nº 1");
                    break;
                case 2:
                    Console.WriteLine("\tHa elegido la opción nº 2");
                    break;
                case 3:
                    Console.WriteLine("\tHa elegido la opción nº 3");
                    break;
                case 4:
                    Console.WriteLine("\tHa elegido la opción nº 4");
                    break;
                case 5:
                    Console.WriteLine("\tHa elegido la opción nº 5");
                    break;
                case 0:
                    break;

                default:
                    Console.WriteLine("\t ERROR: opción inválida");
                    break;
            }



            Console.Write("\n\t Pulse intro para salir"); //salida
            Console.ReadLine();



        }

    }
}

