﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_fechaAvanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            bool correcto = false;

            Console.Write("Introduzca el año: ");
            int anyo = Convert.ToInt32(Console.ReadLine());
            Console.Write("Introduzca el mes: ");
            int mes = Convert.ToInt32(Console.ReadLine());


            if (mes > 0 && mes < 13)
            {
                correcto = false;
                Console.Write("Introduzca el dia: ");
                int dia = Convert.ToInt32(Console.ReadLine());

                if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
                {
                    if (dia > 0 && dia < 32) correcto = true;
                }
                else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                {
                    if (dia > 0 && dia < 31) correcto = true;
                }
                else if ((anyo % 4 == 0 && anyo % 100 != 0) || (anyo % 4 == 0 && anyo % 400 == 0 && anyo % 100 == 0))
                {
                    if (dia > 0 && dia < 30) correcto = true;
                }
                else if (dia > 0 && dia < 29) correcto = true;

                if (correcto == true)
                {
                    Console.Write("La fecha es:  {0} de ", dia);

                    if (mes == 1) Console.Write("enero");
                    else if (mes == 2) Console.Write("febrero");
                    else if (mes == 3) Console.Write("marzo");
                    else if (mes == 4) Console.Write("abril");
                    else if (mes == 5) Console.Write("mayo");
                    else if (mes == 6) Console.Write("junio");
                    else if (mes == 7) Console.Write("julio");
                    else if (mes == 8) Console.Write("agosto");
                    else if (mes == 9) Console.Write("septiembre");
                    else if (mes == 10) Console.Write("octubre");
                    else if (mes == 11) Console.Write("noviembre");
                    else Console.Write("diciembre");

                    Console.WriteLine(" del {0}.", anyo);
                }
                else Console.WriteLine("Dia fuera de rango");

            }
            else Console.WriteLine("Mes no valido");




            Console.WriteLine("Pulse intro para salir:");
            Console.ReadLine();
        }
    }
}
