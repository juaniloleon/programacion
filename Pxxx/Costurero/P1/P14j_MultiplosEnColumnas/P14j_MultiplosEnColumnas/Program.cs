﻿/*
Alumno: Leon Oyola, Juan Antonio


14j. MultiplosEnColumnas:
Se pide un número cant, entre 100 y 300, un
número inicial ni, entre 1000 y 2000, un número
mul, entre 11 y 77 y un número nc, entre 3 y 9. El
programa presentará los cant múltiplos de mul, a
partir de ni en nc columnas.
Ejemplo: si cant= 200, ni = 1500, mul=30 y nc =6,
el programa presentará los 200 primeros múltiplos de
30 a partir del 1500 en 6 columnas


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14j_MultiplosEnColumnas
{
    class Program
    {

        private static int pidoNumero(int min, int max, string enunciado)
        {
            int num;
            bool esCorrecto;
            do  //pido cant
            {
                Console.Write(enunciado + "  [{0}...{1}]:  ", min, max);
                if (!(esCorrecto = Int32.TryParse(Console.ReadLine(), out num)))
                    Console.WriteLine("\t\tError, entrada no numerica......", min, max);

                if ((num > max || num < min) && esCorrecto)
                {
                    esCorrecto = false;
                    Console.WriteLine("\t\tError, valor incorrecto......", min, max);
                }

            } while (!esCorrecto);

            return num;

        }

        private static void imprime(int ni, int mul, int cant, int nc)
        {
            if (ni % mul != 0)  //busco el primero
                ni = ni - (ni % mul) + mul;

            for (int i = 0; i < cant; i++)          //imprimo en bonito
            {
                Console.Write((i + 1) + ") " + ni + "\t");
                if (i < 9)   //no se el motivo, pero los 9 primero no salen bien sin esto.
                    Console.Write("\t");
                ni += mul;
                if ((i + 1) % nc == 0)
                    Console.WriteLine("\n");
            }


        }
        static void Main(string[] args)
        {


            int cant = pidoNumero(100, 300, "\n\tIntroduzca la cantidad");
            int ni = pidoNumero(1000, 2000, "\n\tIntroduzca un numero inicial");
            int mul = pidoNumero(11, 77, "\n\tIntroduzca el numero de multiplos");
            int nc = pidoNumero(3, 9, "\n\tIntroduzca el numero de columnas");

            imprime(ni, mul, cant, nc);

            Console.ReadKey(); //salida

        }

    }

}
