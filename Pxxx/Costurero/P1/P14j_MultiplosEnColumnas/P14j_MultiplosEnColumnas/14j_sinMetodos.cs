﻿/*
Alumno: Leon Oyola, Juan Antonio


14j. MultiplosEnColumnas:
Se pide un número cant, entre 100 y 300, un
número inicial ni, entre 1000 y 2000, un número
mul, entre 11 y 77 y un número nc, entre 3 y 9. El
programa presentará los cant múltiplos de mul, a
partir de ni en nc columnas.
Ejemplo: si cant= 200, ni = 1500, mul=30 y nc =6,
el programa presentará los 200 primeros múltiplos de
30 a partir del 1500 en 6 columnas


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14j_MultiplosEnColumnas
{
    class Program
    {
        static void Main(string[] args)
        {
            int cant, ni, mul, nc;
            bool esCorrecto=false;

            do  //pido cant
            {
                Console.Write("\n\tIntroduzca la cantidad, entre 100 y 300: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out cant);

                if (cant > 300 || cant < 100 || esCorrecto == false)
                    Console.WriteLine("Error, valor fuera de rango......");
            } while (cant > 300 || cant < 100 || esCorrecto == false);

            do //pido ni
            {
                Console.Write("\n\tIntroduzca un numero inicial entre 1000 y 2000: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out ni);
                if (ni > 2000 || ni < 1000 || esCorrecto == false)
                    Console.WriteLine("Error, valor fuera de rango......");
            } while (ni > 2000 || ni < 1000 || esCorrecto == false);


            do  //pido mul
            {
                Console.Write("\n\tIntroduzca el numero de multiplos, entre 11 y 77: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out mul);
              //  mul = Convert.ToInt32(Console.ReadLine());
                if (mul > 77 || mul < 11 || esCorrecto == false)
                    Console.WriteLine("Error, valor fuera de rango......");
            } while (mul > 77 || mul < 11 || esCorrecto == false);


            do  //pido nc
            {
                Console.Write("\n\tIntroduzca el numero de columnas entre 3 y 9: ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out nc);
                if (nc >9 || nc < 3 || esCorrecto == false)
                    Console.WriteLine("Error, valor fuera de rango......");
            } while (nc > 9 || nc < 3 || esCorrecto == false);




            if (ni % mul != 0)  //busco el primero
                ni = ni - (ni % mul) + mul;

           for (int i=0; i< cant; i++)          //imprimo en bonito
            {
                Console.Write((i + 1) + ") " + ni+"\t");
                if (i <9)   //no se el motivo, pero los 9 primero no salen bien sin esto.
                    Console.Write("\t");
                ni += mul;
                if ((i + 1) % nc == 0)
                    Console.WriteLine("\n");
            }

            Console.ReadKey();      //salida

        }
    }
}
