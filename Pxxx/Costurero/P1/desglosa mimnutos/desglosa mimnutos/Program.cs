﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace desglosa_mimnutos
{
    class Program
    {
        static void Main(string[] args)
        {
            string n2;
            do
            {
                Console.Clear();
                string n;
                Console.WriteLine("1 - Suma\n2 - Resta\n3 - Multiplicación\n4 - División\n\n0 - Salir del programa");
                Console.Write("\nIntroduzca una opción: ");
                n = Console.ReadLine();
                while (n != "0" && n != "1" && n != "2" && n != "3" && n != "4")
                {
                    Console.Write("\nPor favor, introduzca una opción correcta: ");
                    n = Console.ReadLine();
                }
                n2 = n;
                switch (n)
                {
                    case "1":
                        suma();
                        break;
                    case "2":
                        resta();
                        break;
                    case "3":
                        multipl();
                        break;
                    case "4":
                        divis_ent();
                        break;
                }
            } while (n2 != "0");

        }


        static void suma()
        {
            string continuar;
            bool error;
            do
            {
                Console.Clear();
                Console.WriteLine("SUMAS");
                int n1, n2, resultado, resulusuario = 0;
                Random r = new Random();
                n1 = r.Next(1, 51);
                n2 = r.Next(1, 51);
                resultado = n1 + n2;

                do
                {
                    try
                    {
                        Console.Write("{0} + {1} = ", n1, n2);
                        resulusuario = int.Parse(Console.ReadLine());
                        error = false;
                    }
                    catch
                    {
                        Console.Write("\nPor favor, introduzca un número entero: ");
                        error = true;
                    }
                } while (error);
                if (resultado == resulusuario)
                {
                    Console.Write("\n¡Has acertado!");
                }
                else
                {
                    Console.Write("\n** ERROR ** El resultado correcto es: {0}", resultado);
                }
                Console.Write("\n\n¿Desea continuar (S/N)? ");
                continuar = Console.ReadLine();
                while (continuar != "s" && continuar != "S" && continuar != "n" && continuar != "N")
                {
                    Console.Write("\nPor favor, introduzca una respuesta válida. ¿Desea continuar (S/N)? ");
                    continuar = Console.ReadLine();
                }
            } while (continuar == "s" || continuar == "S");
        }
        static void resta()
        {
            string continuar;
            bool error;
            do
            {
                Console.Clear();
                Console.WriteLine("RESTAS");
                int n1, n2, resultado, resulusuario = 0;
                Random r = new Random();
                n1 = r.Next(1, 51);
                n2 = r.Next(1, 51);
                resultado = n1 - n2;
                do
                {
                    try
                    {
                        Console.Write("{0} - {1} = ", n1, n2);
                        resulusuario = int.Parse(Console.ReadLine());
                        error = false;
                    }
                    catch
                    {
                        Console.Write("\nPor favor, introduzca un número entero: ");
                        error = true;
                    }
                } while (error);
                if (resultado == resulusuario)
                {
                    Console.Write("\n¡Has acertado!");
                }
                else
                {
                    Console.Write("\n** ERROR ** El resultado correcto es: {0}", resultado);
                }
                Console.Write("\n\n¿Desea continuar (S/N)? ");
                continuar = Console.ReadLine();
                while (continuar != "s" && continuar != "S" && continuar != "n" && continuar != "N")
                {
                    Console.Write("\nPor favor, introduzca una respuesta válida. ¿Desea continuar (S/N)? ");
                    continuar = Console.ReadLine();
                }
            } while (continuar == "s" || continuar == "S");
        }
        static void multipl()
        {
            string continuar;
            bool error;
            do
            {
                Console.Clear();
                Console.WriteLine("MULTIPLICACIONES");
                int n1, n2, resultado, resulusuario = 0;
                Random r = new Random();
                n1 = r.Next(1, 51);
                n2 = r.Next(1, 51);
                resultado = n1 * n2;
                do
                {
                    try
                    {
                        Console.Write("{0} * {1} = ", n1, n2);
                        resulusuario = int.Parse(Console.ReadLine());
                        error = false;
                    }
                    catch
                    {
                        Console.Write("\nPor favor, introduzca un número entero: ");
                        error = true;
                    }
                } while (error);
                if (resultado == resulusuario)
                {
                    Console.Write("\n¡Has acertado!");
                }
                else
                {
                    Console.Write("\n** ERROR ** El resultado correcto es: {0}", resultado);
                }
                Console.Write("\n\n¿Desea continuar (S/N)? ");
                continuar = Console.ReadLine();
                while (continuar != "s" && continuar != "S" && continuar != "n" && continuar != "N")
                {
                    Console.Write("\nPor favor, introduzca una respuesta válida. ¿Desea continuar (S/N)? ");
                    continuar = Console.ReadLine();
                }
            } while (continuar == "s" || continuar == "S");
        }
        static void divis_ent()
        {
            string continuar;
            bool error;
            do
            {
                Console.Clear();
                Console.WriteLine("DIVISIONES");
                int n1, n2, resultado, resulusuario = 0;
                Random r = new Random();
                n1 = r.Next(1, 51);
                n2 = r.Next(1, 51);
                resultado = n1 / n2;
                do
                {
                    try
                    {
                        Console.Write("{0} / {1} = ", n1, n2);
                        resulusuario = int.Parse(Console.ReadLine());
                        error = false;
                    }
                    catch
                    {
                        Console.Write("\nPor favor, introduzca un número entero: ");
                        error = true;
                    }
                } while (error);
                if (resultado == resulusuario)
                {
                    Console.Write("\n¡Has acertado!");
                }
                else
                {
                    Console.Write("\n** ERROR ** El resultado correcto es: {0}", resultado);
                }
                Console.Write("\n\n¿Desea continuar (S/N)? ");
                continuar = Console.ReadLine();
                while (continuar != "s" && continuar != "S" && continuar != "n" && continuar != "N")
                {
                    Console.Write("\nPor favor, introduzca una respuesta válida. ¿Desea continuar (S/N)? ");
                    continuar = Console.ReadLine();
                }
            } while (continuar == "s" || continuar == "S");
        }
    }
}
