﻿/*
80 primeros multiplos de num a partir de 800
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14_a6_Multiplos_entre_limites
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Numero: ");

            int num = Convert.ToInt32(Console.ReadLine());
            while (num < 10 || num > 99)
            {
                Console.Write("EROR_numero no valido- Numero: ");

                num = Convert.ToInt32(Console.ReadLine());
            }


            int primer = num * (700 / num);
            if (primer <= 700)
                primer += num;

            int aux = primer;
            int i = 0;

            Console.Write("\n\n\tWHILE: [INTRO]");
            Console.ReadLine();


            while (i < 80)
            {
                Console.Write(aux + "\t");
                aux = aux + num;
                i++;

            }

            Console.Write("\n\n\tDO-WHILE: [INTRO]");
            Console.ReadLine();


            aux = primer;
            i = 0;
            do
            {
                Console.Write(aux + "\t");
                aux = aux + num;
                i++;

            } while (i < 80);


            Console.Write("\n\n\tFOR: [INTRO]");
            Console.ReadLine();

            aux = primer;
            for (i = 0; i < 80; i++)
            {
                Console.Write(aux + "\t");
                aux += num;


            }


            Console.Write("\n\n\tSALIR");
            Console.ReadLine();



        }
    }
}

