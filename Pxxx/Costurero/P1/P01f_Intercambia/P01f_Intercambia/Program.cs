﻿// Alumno: Leon Oyola, Juan Antonio

/* P01f_Intercambia
Se introducen los valores de dos variables, x e y e intercambia sus valores. 

Para comprobar que el intercambio se ha realizado correctamente, se escribirñan sus calores, antes y depués del intercambios.

Dos verisones, con enteros y con string
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P01f_Intercambia
{
    class Program
    {
        static void Main(string[] args)
        {
            int x, y, aux, selector;
            string X, Y, AUX;

            Console.Write("Presione 1 para enteros o 0 para strings:");
            selector = Convert.ToInt32(Console.ReadLine());

            if (selector == 1)
            {
                Console.Write("Introduzca unicamente valores enteros\n");

                Console.Write("Introduzca valor de X: ");
                x = Convert.ToInt32(Console.ReadLine());
                Console.Write("Introduzca valor de Y: ");
                y = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Antes de la conversión. X:{0} Y:{1}", x, y);
                aux = y;
                y = x;
                x = aux;
                Console.WriteLine("Antes de la conversión. X:{0} Y:{1}", x, y);

            }
            else if (selector == 0)
            {
                Console.Write("Introduzca unicamente cadenas de caracteres\n");


                Console.Write("Introduzca valor de X: ");
                X = Console.ReadLine();
                Console.Write("Introduzca valor de Y: ");
                Y = Console.ReadLine();

                Console.WriteLine("Antes de la conversión. \nX: '{0}' \nY: '{1}'", X, Y);
                AUX = Y;
                Y = X;
                X = AUX;
                Console.WriteLine("\n\nDespues de la conversión. \nX: '{0}' \nY: '{1}'", X, Y);
            }
            else
            {
                Console.WriteLine("Valor no válido.\n\n");

            }


            Console.Write("Intro para salir...");
            Console.ReadLine();

        }
    }
}
