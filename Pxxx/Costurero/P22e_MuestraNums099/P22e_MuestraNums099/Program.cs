﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void MuestraNums099()
    {
        int decena = 0;

        for (int i = 0; i < 100; i++)
        {
            int columna = 3 + 5 * (i - decena*10);
            int fila = 2 + 2 * decena;

            if ((i + 1) % 10 == 0)
            {
                decena += 1;
                               
            }


            Console.SetCursorPosition(columna,fila);
            Console.Write(i);

            if ((i + 1) % 10 == 0)
                Console.Beep(5648, 100);
            else
            Console.Beep((100- i)*100, 100);

        }

    }

    static void Main(string[] args)
    {
        Console.WriteLine("Pulse cualquier tecla para mostrar los numerosdel 0 al 99.\n\n");
        Console.ReadKey();
        Console.Clear();

        MuestraNums099();

        Console.WriteLine("\n\n\nPulse cualquier tecla para salir");
        Console.ReadKey();
    }
}

