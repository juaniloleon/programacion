﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P23a_AciertaNumero2
{
    class Program
    {
        static int CapturaEntero(string frase, int min, int max)
        {
            int resultado;
            bool esCorrecto;
            do
            {
                Console.Write(frase + "[ " + min + " hasta " + max + "] : ");
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (!esCorrecto)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n ERROR: El valor que has introducido no es válido");

                else if (resultado > max || resultado < min)//compruebo el error del maximo y minimo
                {
                    Console.WriteLine("\n ERROR: El número que has introducido no está en el rango");
                    esCorrecto = false;
                }
            } while (!esCorrecto);

            return resultado;
        }

        static void MuestraTabla(int[] tabla)
        {
            // Mostremos todos los elementos de la tabla
            for (int i = 0; i < tabla.Length; i++)
            {
                // mostramos cada posición con su valor
                Console.WriteLine("{0}) {1}", i, tabla[i]);
            }
        }

        static void Main(string[] args)
        {
            //Declaramos una tabla de enteros
            int[] tablaEnteros;
            int i;

            // Preguntamos al usuario el número de elementos, entre 10 y 100
            int tamanyo = CapturaEntero("¿Número de elementos del vector?: ", 10, 100);

            // Construyo la tabla antes declarada
            tablaEnteros = new int[tamanyo];

            // Mostremos todos los elementos de la tabla, que serán ceros
            MuestraTabla(tablaEnteros);

            Console.WriteLine("Vamos a cargar los 4 primeros valores de la tabla manualmente\ncon valores de dos cifras");
            for (i = 0; i < 4; i++)
            {
                tablaEnteros[i] = CapturaEntero("\n\t¿El valor " + (i + 1) + "º?: ", 10, 99);
            }

            // Mostremos todos los elementos de la tabla
            MuestraTabla(tablaEnteros);

            Console.WriteLine("Pulsa una tecla para cargar los demás valores de la tabla\n con valores de dos cifras tomados al azar");
            Console.ReadKey(true);

            Random azar = new Random();
            for (i = 4; i < tablaEnteros.Length; i++)
            {
                tablaEnteros[i] = azar.Next(10, 100);
            }
            // Mostremos todos los elementos de la tabla
            MuestraTabla(tablaEnteros);

            // cambiar valores
            int id;

            id = CapturaEntero("\n\tPosición de la tabla a cambiar (0 para salir): ", 0, tablaEnteros.Length - 1);
            while (id != 0)
            {
                // Presentamos el valor actual
                Console.Write("\n\tValor actual: " + tablaEnteros[id]);
                // Capturamos el nuevo valor de tres cifras
                tablaEnteros[id] = CapturaEntero("\t¿Nuevo Valor? ", 100, 999);
                id = CapturaEntero("\n\tPosición de la tabla a cambiar (0 para salir): ", 0, tablaEnteros.Length - 1);
            }
            MuestraTabla(tablaEnteros);

            // Buscar valores en la tabla y decimos en qué posición está o si que no existe en la tabla
            // Búsqueda secuencial SIN garantía de éxito
            int valor;
            valor = CapturaEntero("\n\tValor a buscar (0 para salir): ", 0, 1000);
            while (valor != 0)
            {
                for (i = 0; i < tablaEnteros.Length; i++)
                {
                    if (tablaEnteros[i] == valor)
                    {
                        Console.WriteLine("\n\tEl número {0} está en la posición {1}.", valor, i);
                        break; // <-- como lo he encontrado no sigo dando vueltas
                    }
                }

                if (i == tablaEnteros.Length) // si se ha pasado del índice de la tabla es que no existe 
                    Console.WriteLine("\n\tEl número {0} No existe en la tabla.", valor);

                valor = CapturaEntero("\n\tOtro Valor (0 para salir): ", 0, 1000);
            }


            Console.WriteLine("\n\n Pulsa intro");
            Console.ReadLine();

        }
    }
}
