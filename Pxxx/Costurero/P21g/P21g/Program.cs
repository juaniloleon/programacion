﻿/*
Alumno: Leon Oyola, Juan Antonio

P21g_MultiplicarSumando:

Una funcion que

Recibe dos enteros, a y b

Multiplica sin usar la multiplicación

Devuelve el producto calculado

Numeros entre 0 y 10000
Optimizada.

*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    private static void Salida()
    {
        Console.Write("\n\n\t\t");
        Console.ForegroundColor = ConsoleColor.DarkGray;
        Console.BackgroundColor = ConsoleColor.White;
        Console.WriteLine(" -> presione cualquier tecla para salir <- ");
        Console.ReadKey();

    }

    private static int MultiplicaSumando(int a, int b)
    {
        int aux=a;

        if (a>b)
        {
            a = b;
            b = aux;
        }

        int total = 0;
        for (int i = 0; i < a ; i++, Console.WriteLine(i))
            total += b;
        return total;
    }

    static void Main(string[] args)
    {
        int num1, num2, mult;
        do
        {
            num1 = CapturaEntero("Introduzca un numero", 0, 10000);
            if (num1 != 0)
            {
                num2 = CapturaEntero("Introduzca otro numero", 0, 10000);

                mult = MultiplicaSumando(num1, num2);

                Console.WriteLine("\n\tEl producto es: " + mult);

            }
        }
        while (num1 != 0);


        Salida();
    }


}