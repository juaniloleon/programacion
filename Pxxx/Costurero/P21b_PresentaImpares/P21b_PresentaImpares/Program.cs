﻿/*
Alumno: Leon Oyola, Juan Antonio

P21b_MetodoPresentaImpares

Método que 
Recibe: dos enteros, min y max.
Hace: presenta los imapres entre min y max, sin incluirlos.
Devuelve: nada.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P21b_PresentaImpares
{
    class Program
    {
        static void PresentaImpares(int min, int max)
        {

            if (min % 2 != 0)
                min += 2;
            else min++;


            for (int i = min; i < max; i += 2)
                Console.Write(i + "\t");

        }
        private static int CapturaEntero()
        {
            bool esCorrecto;
            int resultado;
            do
            {
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **\n\t Introduzca:");
            } while (esCorrecto == false);

            return resultado;
        }
        private static int CapturaEnteroMin(int min)
        {
            bool esCorrecto;
            int resultado;
            do
            {
                esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
                if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                    Console.WriteLine("\n** Error: Valor introducido no válido **\n\t Introduzca:");
                else if (resultado < min)
                {
                    Console.WriteLine("\n** Error: El número es inferior al minimo ({0}) **\n\t Introduzca:", min);
                    esCorrecto = false;
                }
            } while (esCorrecto == false);

            return resultado;
        }

        static void Main(string[] args)
        {
            senyera();
            Console.BackgroundColor = ConsoleColor.Black;


            Console.Write("\t\tintroduzca valor minimo: ");
            int min = CapturaEntero();
            Console.Write("\t\tintroduzca valor maximo: ");

            int max = CapturaEnteroMin(min);

            PresentaImpares(min, max);


            Salida();
        }

        private static void Salida()
        {
            Console.Write("\n\n\t\t");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.WriteLine("presione cualquier tecla para salir");
            Console.ReadKey();

        }

        private static void senyera()
        {
            Console.ForegroundColor = ConsoleColor.White;

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("               \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("  ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("              \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("   ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("             \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("    ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("            \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("  *  ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("           \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("    ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("            \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("   ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("             \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write("  ");
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("              \t");

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.Write(" ");
            Console.BackgroundColor = ConsoleColor.Yellow;
            Console.WriteLine("               \t");

        }

    }
}
