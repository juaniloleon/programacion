﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

  class Program
    {
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

    private static int CapturaEntero(string texto)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0}: ", texto);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");

        } while (esCorrecto == false);

        return resultado;
    }

    static void Main(string[] args)
        {
        Random rnd = new Random();

        int[] tablaEntero= new int[CapturaEntero("Introduzca tamaño de tabla", 0,10)];

        Console.WriteLine("\n\tla tabla tiene un tamaño de {0} enteros", tablaEntero.Length);

        for (int i=0; i< tablaEntero.Length; i++)
       {

            tablaEntero[i] = rnd.Next(10, 100);
        }

        for (int i = 0; i < 4; i++)
        {
            tablaEntero[i] = CapturaEntero("Dame un valor", 10,99);
        }
        for (int i = 4; i < tablaEntero.Length; i++)
        {

            tablaEntero[i] = rnd.Next(10, 100);
        }


        Console.WriteLine("El contenido de la tabla es:");
        for (int i = 0; i < tablaEntero.Length; i++)
        {
            Console.WriteLine("\n\ttabla[{0}]={1}", i, tablaEntero[i]);
        }


        Console.ReadKey();
        
        }
    }
