﻿/*
Alumno: Leon Oyola, Juan Antonio

1)
A partir de los apellidos y nombres que tienes más abajo, 
construirrespectivamente los vectorestApellidosy tNombres.
2)
A continuación construye la tabla tab2dGente —con las mismas filas y dos columnas—y cárgalas
colocando para cada alumno el nombre en la primera columna y los apellidos en la segunda. 
3)
Luego presentar «Nombre Apellidos»de cada personaa partir de la tabla tab2dGente
4)Construye el vector de stringtabApellNomby rellénala con los 
“Apellidos, Nombres” tomándolos de la tabla tab2dGente
.
5)Muestra tabApellNomben pantalla.
6)
Muestra la persona cuyos Apellidos, Nombre tiene más caracteres.
Nota: entre un paso y el siguiente pon una pausa en la que el usuario tenga que
pulsar una tecla
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.White;

        TextoColor("Pulse cualquier tecla para cargar las tablas de Apellidos y Nombres: ");

        string[] tApellidos = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
        string[] tNombres = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Angel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafael", "Antonio" };

        TextoColor("Pulse cualquier tecla para crear tab2dGente: ");
        string[,] tab2dGente = CombinaTabla(tNombres, tApellidos);

        TextoColor("Pulse cualquier tecla para ver los Nombres y Apellidos: ");  
        ImprimeteTabla2D(tab2dGente);

        TextoColor("Pulse cualquier tecla para crear tabApellNomb: ");       
        string[] tabApellNomb = ConcatenaTabla(tNombres, tApellidos);

        TextoColor("Pulse cualquier tecla para ver tabApellNomb: ");
        ImprimeteTabla(tabApellNomb);

        TextoColor("Pulse cualquier tecla para mostrar el que más carácteres tiene: ");      
        Mayor(tabApellNomb);
        
        TextoColor("Pulse cualquier tecla para salir: ");

    }

    private static void TextoColor(string v)
    {
        Console.BackgroundColor = ConsoleColor.Blue;
        Console.WriteLine(v);
        Console.BackgroundColor = ConsoleColor.Black;
        Console.ReadKey(true);

    }

    private static void Mayor(string[] tabApellNomb)
    {
        string mayor = "";

        for (int i = 0; i < tabApellNomb.Length; i++)
        {
            if (tabApellNomb[i].Length > mayor.Length)
                mayor = tabApellNomb[i];
        }

        Console.WriteLine(mayor);

    }

    private static string[] ConcatenaTabla(string[] tNombres, string[] tApellidos)
    {
        string[] tabNomAp = new string[tNombres.Length];

        for (int i = 0; i < tabNomAp.Length; i++)
            tabNomAp[i] = (tApellidos[i] + ", " + tNombres[i]);


        return tabNomAp;
    }

    private static void ImprimeteTabla2D(string[,] tab2dGente)
    {

        for (int i = 0; i < tab2dGente.GetLength(0); i++)
        {
            Console.Write("\t {0}) ",i);
            for (int j = 0; j < 2; j++)
            {
                Console.Write(tab2dGente[i, j] + " ");
            }
            Console.WriteLine();
        }
    }
    private static void ImprimeteTabla(string[] tabla)
    {

        for (int i = 0; i < tabla.GetLength(0); i++)
            Console.WriteLine("\t {0}) {1}",i, tabla[i]);

    }

    private static string[,] CombinaTabla(string[] tNombres, string[] tApellidos)
    {
        string[,] tab2dGente = new string[tNombres.Length, 2];

        for (int i = 0; i < tNombres.Length; i++)
        {
            tab2dGente[i, 0] = tNombres[i];
            tab2dGente[i, 1] = tApellidos[i];

        }

        return tab2dGente;
    }
}