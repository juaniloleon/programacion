﻿/*

P25a_Tabla2dDoubles
Alumno: León Oyola, Juan Antonio

Tabla 10*7 valores double 00.00 - 99.99 rnd

pregunta si quiere cambiar alguno, se le pasa coordenads y cambia en w sobre r
al responder no finaliza


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.White;

        char input;
        int filas = 10, fila;  // CapturaEntero("Numero de filas", 1, 100);
        int columnas = 7, columna;  // CapturaEntero("Numero de columnas", 1, 100);
        double[,] tabla = new double[filas, columnas];
        bool modificar = false;

        tabla = RellenaTabla(tabla);
        ImprimeTabla(tabla);

        do
        {
            Console.SetCursorPosition(2, 24);

            Console.Write("¿Quiere modificar algún valor? (y/n): ");
            input = Console.ReadKey(true).KeyChar;
            if (input == 'y' || input == 's' || input == 'Y' || input == 'S')
            {
                modificar = true;
                Limpia();
                fila = CapturaEntero("Fila: ", 0, (tabla.GetLength(0) - 1));
                columna = CapturaEntero("Columna: ", 0, (tabla.GetLength(1) - 1));
                tabla = EditaTabla(tabla, fila, columna);
                Limpia();

            }
            else if (input == 'n' || input == 'N')
                modificar = false;

            else modificar = true;

        } while (modificar);

        Console.Clear();
        Console.Write("\n\nSaliendo...");
        Console.ReadKey(true);
    }

    private static void Limpia()
    {
        Console.SetCursorPosition(2, 24);
        Console.Write("\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n\t\t\t\t\t\t\n");
        Console.SetCursorPosition(2, 24);
    }

    private static double[,] EditaTabla(double[,] tabla, int fila, int columna)
    {
        double valor = CapturaDouble("Nuevo valor: ", 00.00, 99.99);
        tabla[fila, columna] = valor;

        Console.BackgroundColor = ConsoleColor.Red;
        Console.SetCursorPosition((7 * (columna + 1)), (2 * (fila + 2)));
        Console.Write("{0}", tabla[fila, columna].ToString("00.00"));
        Console.BackgroundColor = ConsoleColor.Black;


        return tabla;
    }

    private static void ImprimeTabla(int[,] tabla2D)
    {


        for (int i = 0; i < tabla2D.GetLength(0); i++)
        {
            for (int j = 0; j < tabla2D.GetLength(1); j++)
            {
                Console.Write("\t" + tabla2D[i, j]);
            }
            Console.WriteLine("\n");

        }
    }
    private static void ImprimeTabla(double[,] tabla2D)
    {
        for (int i = 0; i < tabla2D.GetLength(0); i++)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.SetCursorPosition((0), (2 * (i + 2)));
            Console.Write("  {0}  ", i);

            if (i < tabla2D.GetLength(1))
            {

                Console.SetCursorPosition((7 * (i + 1)), (2));
                Console.Write("  {0}  ", i);
            }
            Console.BackgroundColor = ConsoleColor.Black;
            for (int j = 0; j < tabla2D.GetLength(1); j++)
            {

                //  Console.SetCursorPosition(columna, fila);
                Console.SetCursorPosition((7 * (j + 1)), (2 * (i + 2)));

                Console.Write("{0}", tabla2D[i, j].ToString("00.00"));
            }

        }

    }
    private static double[,] RellenaTabla(double[,] tabla)
    {
        Random rnd = new Random();
        for (int i = 0; i < tabla.GetLength(0); i++)
        {
            for (int j = 0; j < tabla.GetLength(1); j++)
            {
                // tabla[i, j] = CapturaEntero("Valor para [" + i + "," + j + "]: ", 0, 100);
                //tabla[i,j](rnd.Next(10000) / 100.00);
                tabla[i, j] = Math.Round((rnd.NextDouble() * 100), 2);

            }
        }
        return tabla;
    }
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    private static double CapturaDouble(string texto, double min, double max)
    {
        bool esCorrecto;
        double resultado;
        do
        {
            Console.Write("\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Double.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un double
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }

}