﻿/*
Alumno: Leon Oyola, Juan Antonio

P24c_DiaDeLaSemana:
Se le pide al usuario que introduzca un número entre 0 y 6, cada uno de los cuales indica un día de la semana en que nos encontramos [0 Domingo, 1 Lunes, … 6 Sábado]. Luego pregunta cuántos días quiere avanzar (numDias) y el programa calcula y muestra en qué día de la semana caerá dentro de numDias.
Nota: Se adjunta el código de la P13b. Como verás, esta práctica es la misma, pero en aquel momento no conocíamos las tablas ni los métodos.
Ahora debes resolverla de una forma mucho más simple.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);

            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            Console.ForegroundColor = ConsoleColor.Red;

            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
            Console.ForegroundColor = ConsoleColor.Gray;

        } while (esCorrecto == false);

        return resultado;
    }

    private static int CapturaEntero(string texto)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0}: ", texto);

            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            Console.ForegroundColor = ConsoleColor.Red;

            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            Console.ForegroundColor = ConsoleColor.Gray;
        } while (esCorrecto == false);

        return resultado;
    }

    static void Main(string[] args)
    {
        string[] diaSemana = { "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" };

        int ini = CapturaEntero("\n\tIntroduce un dia de la semana: ", 0, 6);

        Console.WriteLine("\n\tHoy es {0}", diaSemana[ini]);

        int avanza = CapturaEntero("\n\tIntroduce cuántos días quieres avanzar");

        int final = (avanza + ini) % 7;

        Console.WriteLine("\n\n\tDentro de {0} dias será {1}", avanza, diaSemana[final]);
        
        Console.ReadKey(true);

    }
}
