﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void MuestraNums099()
    {
        for (int i = 0; i < 100; i++)
        {
            ColocaElNumero(i);
           // Sonidito(i);
        }


    }

    private static void Sonidito(int i)
    {
        if ((i + 1) % 10 == 0)
            Console.Beep(5648, 100);
        else
            Console.Beep((100 - i) * 100, 100);
    }

    private static void ColocaElNumero(int num)
    {

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;
        
        Console.SetCursorPosition(columna, fila);
        Console.Write(num);


    }

    static void Main(string[] args)
    {
        Console.WriteLine("Pulse cualquier tecla para mostrar los numerosdel 0 al 99.\n\n");
        Console.ReadKey();
        Console.Clear();

        MuestraNums099();

        Console.WriteLine("\n\n\nPulse cualquier tecla para salir");
        Console.ReadKey();
    }
}