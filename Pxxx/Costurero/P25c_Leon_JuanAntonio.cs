﻿/*
 P25c_NotasAlumnos: 

 Carga tApellidos y tNombres, monta tAlumnos,
 Crea tNotas con notas aleatorias de 1 decimal
muestra id  Alumno  Prog    ED  BD  Media

AV: el id es un aleatorio de dos cifras sin repetir.
 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static void Main(string[] args)
    {
        string[] tApellidos = { "Sánchez Elegante", "Arenas Mata", "García Solís", "Rodríguez Vázquez", "Hurtado Miranda", "Pinto Mirinda", "Barrios Garrobo", "Márquez Salazar", "Medina Gómez", "Alonso Pérez", "López Mora", "González Chaparro", "Ferrer Jiménez", "Morales Moncayo", "Fernández Perea", "Blanco Roldán", "Navarro Romero", "Aguilar Rubio", "Baena Fernández", "Barco Ramírez", "Delgado Rodríguez", "Duque Martínez" };
        string[] tNombres = { "Álvaro", "Daniel Luis", "Juan Manuel", "Agustín", "Fco. Javier", "José Manuel", "Tomás", "Carlos", "Jose Carlos", "Juan Luis", "Daniel", "Angel", "Jacobo", "Alejandro", "Francisco", "Alfredo", "Francisco", "Antonio", "Constantino", "Roberto", "Rafael", "Antonio" };
        string[] tAlumnos = ConcatenaTabla(tNombres, tApellidos);

        float[,] tNotas = new float[tAlumnos.Length, 3];
        tNotas = IniciaTabla(tNotas);

        ImprimeteNotas(tAlumnos, tNotas);


        Console.ReadKey(true);

    }

    private static void ImprimeteNotas(string[] tAlumnos, float[,] tNotas)
    {
        int[] id = IDrandom(tAlumnos.Length);

        float media;
        Console.WriteLine("id\tAlumno\t\t\t\tProg\tED\tBD\tMedia\n");

        for (int i = 0; i < tAlumnos.GetLength(0); i++)
        {
            media = 0;
            Console.Write("{0})\t{1}", id[i], tAlumnos[i]);

            for (int j = 0; j < tNotas.GetLength(1); j++)
            {
                Console.SetCursorPosition(40 + (8 * j), (2 + i));
                Console.Write("{0}\t", tNotas[i, j]);
                media += tNotas[i, j];
            }
            Console.WriteLine(Math.Round(media / 3, 2));
        }


    }

    private static float[,] IniciaTabla(float[,] tNotas)
    {
        Random rnd = new Random();
        for (int i = 0; i < tNotas.GetLength(0); i++)
            for (int j = 0; j < tNotas.GetLength(1); j++)
                tNotas[i, j] = ((float)rnd.Next(0, 100) / 10);

        return tNotas;
    }

    private static string[] ConcatenaTabla(string[] tNombres, string[] tApellidos)
    {
        string[] tabNomAp = new string[tNombres.Length];

        for (int i = 0; i < tabNomAp.Length; i++)
            tabNomAp[i] = (tApellidos[i] + ", " + tNombres[i]);


        return tabNomAp;
    }
    private static void ImprimeteTabla2D(string[,] tab2dGente)
    {

        for (int i = 0; i < tab2dGente.GetLength(0); i++)
        {
            Console.Write("\t {0}) ", i);
            for (int j = 0; j < tab2dGente.GetLength(1); j++)
            {
                Console.Write(tab2dGente[i, j] + " ");
            }
            Console.WriteLine();
        }
    }

       

    private static int[] IDrandom(int tam)
    {
        int[] tab = new int[tam];
        Random rnd = new Random();

        int aux = 0;
        int cont = 0;

        for (int i = 0; i < tab.Length; i++)
        {
            aux = rnd.Next(10, 100);
            bool continuar = false;

            while (!continuar)
            {
                for (int j = 0; j <= cont; j++)
                    if (aux == tab[j])
                    {
                        continuar = true;
                        j = cont;
                    }

                if (continuar)
                {
                    aux = rnd.Next(10,100);
                    continuar = false;
                }
                else
                {
                    continuar = true;
                    tab[cont] = aux;
                    cont++;
                }
            }
        }

        return tab;
    }
    private static void ImprimeteTabla(int[] tabla)
    {

        for (int i = 0; i < tabla.GetLength(0); i++)
            Console.WriteLine("\t {0}) {1}", i, tabla[i]);

    }
    private static void ImprimeteTabla(string[] tabla)
    {

        for (int i = 0; i < tabla.GetLength(0); i++)
            Console.WriteLine("\t {0}) {1}", i, tabla[i]);

    }

    private static string[,] CombinaTabla(string[] tNombres, string[] tApellidos)
    {
        string[,] tab2dGente = new string[tNombres.Length, 2];

        for (int i = 0; i < tNombres.Length; i++)
        {
            tab2dGente[i, 0] = tNombres[i];
            tab2dGente[i, 1] = tApellidos[i];

        }

        return tab2dGente;
    }



}
