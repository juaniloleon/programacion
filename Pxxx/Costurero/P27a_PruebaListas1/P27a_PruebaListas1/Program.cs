﻿/*

1) Carga una tabla de enteros cuyo tamaño se pide al usuario (10-90)
2) Carga una lista de enteros con los valores anteriores
3) Mostrar en una columna los valores de la tabla
4) Mostrar en pantalla a la derecha de la columna anterior los valores de la Lista
5) Ordenar la lista y mostrarla a la derecha
6) Invertir la lista y mostrar a la derecha


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class Program
{
    static Random rnd = new Random();
    static void Main(string[] args)
    {
        Console.ForegroundColor = ConsoleColor.White;

        int tam = CaputuraEntero("Numero de elementos", 10, 99);
        int[] tabla = tablaAzar(tam);

        List<int> lista = rellenaLista(tabla);


        imprimeEn(" T", 0);
        imprimeTabla(tabla, 0);

        imprimeEn(" L", 1);
        Console.ForegroundColor = ConsoleColor.Magenta;

        imprimeLista(lista, 1);

        Console.ForegroundColor = ConsoleColor.White;

        imprimeEn(" S", 2);
        lista.Sort();
        Console.ForegroundColor = ConsoleColor.Green;

        imprimeLista(lista, 2);
        
        Console.ForegroundColor = ConsoleColor.White;


        imprimeEn(" I", 3);
        lista.Reverse();
        Console.ForegroundColor = ConsoleColor.Cyan;

        imprimeLista(lista, 3);
        Console.ForegroundColor = ConsoleColor.White;


        Console.ReadKey(true);
    }

    private static void imprimeEn(string texto, int columna)
    {
        Console.SetCursorPosition(3 + columna * 7, 3);
        Console.BackgroundColor = ConsoleColor.Red;
        Console.ForegroundColor = ConsoleColor.Black;

        Console.Write(texto);
        Console.ForegroundColor = ConsoleColor.White;

        Console.BackgroundColor = ConsoleColor.Black;

    }

    private static void imprimeLista(List<int> lista, int columna)
    {
        for (int i = 0; i < lista.Count; i++)
        {
            Console.SetCursorPosition(3 + (columna * 7), 5 + i);
            Console.WriteLine(lista[i]);
        }

    }


    private static void imprimeTabla(int[] tabla, int columna)
    {
        Console.WriteLine();
        for (int i = 0; i < tabla.Length; i++)
        {
            Console.SetCursorPosition(3 + (columna * 7), 5 + i);

            Console.WriteLine(tabla[i]);
        }
    }

    private static List<int> rellenaLista(int[] tabla)
    {
        List<int> lista=new List<int>();

        for (int i = 0; i < tabla.Length; i++)
            lista.Add(tabla[i]);

        return lista;
    }

    private static int[] tablaAzar(int tam)
    {
        int[] tabla = new int[tam];

        for (int i=0; i< tam; i++)
           tabla[i] = rnd.Next(10,100);
        

        return tabla;
    }

    private static int CaputuraEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
}

   

