﻿// Alumno: Leon Oyola, Juan Antonio

/* P01c
    Se introduce los tres coeficientes a,b y c (enteros) de la ecuacion de segundo grado (aX^2+bX+c=0) y devuelve las dos soluciones X1 y X2
*/

using System;
using System.Collections.Generic;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        int a, b, c;  //Valores de entrada
        double x1, x2;  //Soluciones

        Console.Write("introduca los coeficientes aX^2 + bX + c = 0 \na: ");   //entrada y conversion de datos
        a = Convert.ToInt32(Console.ReadLine());
        Console.Write("b: ");
        b = Convert.ToInt32(Console.ReadLine());
        Console.Write("c: ");
        c = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Ha introducido {0}X^2 + {1}X + {2}=0", a, b, c);
        Console.WriteLine("Se puede resolver: " + (((b * b) - (4 * a * c))>=0));

        x1 = (-b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);     //calculo de soluciones
        x2 = (-b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

        

        Console.WriteLine("La solución es X1={0} y X2={1}", x1, x2);        //salida de soluciones
                

        Console.Write("\n\n\n\t\tPulsa Intro para salir");
        Console.ReadLine();
    }
}