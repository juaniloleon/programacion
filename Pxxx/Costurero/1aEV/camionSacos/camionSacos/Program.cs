﻿/*

Tenemos una furgoneta que puede transportar hasta 4000Kg

Realiza un programa que pide el peso de un saco (int) y averigua cuantos sacos iguales puede
cargar en la gurgoneta

Solo se permite el uso de la suma y la resta

El usuario escribirá un valor correcto


*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace camionSacos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\t\tIntroduzca peso de sacos:  ");
            int peso=Convert.ToInt32(Console.ReadLine());
            int sacos = 0, i = 0;


            for (i=0; i<4000; i += peso)
            {
                sacos ++;
            }

            if (i > 4000)
                sacos--;
            Console.Write("\n\n\tPuede cargar un total de {0} saco", sacos);

            if (sacos!=1)
                Console.Write("s");
            Console.Write(" de {0} kilo", peso);
            
            if (peso != 1)
                Console.Write("s");

            Console.WriteLine("\n\nPresione cualquier tecla para salir");
            Console.ReadKey();


        }
    }
}
