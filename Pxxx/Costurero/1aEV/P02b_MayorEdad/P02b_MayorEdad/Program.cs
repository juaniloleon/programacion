﻿// Alumno: Leon Oyola, Juan Antonio

/* P02b:MayorEdad
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P02b_MayorEdad
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("\tIntroduzca su nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("\tIntroduzca su edad: ");
            int edad = Convert.ToInt32(Console.ReadLine());

            if (edad >= 18)
                Console.WriteLine("\n\n Buenos días {0}. Ya eres mayor de edad", nombre);
            else
                Console.WriteLine("\n\n Buenos días {0}, te faltan {1} años para ser mayor de edad", nombre, (18-edad));


            Console.Write("\n\n\n Preisone intro para salir");
            Console.ReadLine();
        }
    }
}
