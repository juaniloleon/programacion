﻿/* alumno Leon Oyola, Juan Antonio


P13d_Menú1::
Menu con 4 opciones
0-> salir
1> Ecuacion segundo grado
2->Desglosa Eruros
3->Presenta fehca

Si elige la opcion correcta se limpiará la pantalla y ejecutará la función elegida.


 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P13d_menu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n\n╔════════════════════════════╗");   //Impresion menu
            Console.WriteLine("║           Menú             ║");
            Console.WriteLine("╠════════════════════════════╣");
            Console.WriteLine("║     1) Ec. Segunda Grado   ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     2) Desglosa Euros      ║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     3) Presenta fecha      ║");
            Console.WriteLine("║____________________________║");
            Console.WriteLine("║                            ║");
            Console.WriteLine("║     0) Salir               ║");
            Console.WriteLine("╚════════════════════════════╝");

            int opcion = Convert.ToInt32(Console.ReadLine());   //lecutura opcion

            Console.Clear();
            switch (opcion)                             //bloques 
            {
                case 1:
                    int a, b, c;  //Valores de entrada
                    double x1, x2;  //Soluciones

                    Console.Write("introduca los coeficientes aX^2 + bX + c = 0 \na: ");   //entrada y conversion de datos
                    a = Convert.ToInt32(Console.ReadLine());
                    Console.Write("b: ");
                    b = Convert.ToInt32(Console.ReadLine());
                    Console.Write("c: ");
                    c = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Ha introducido {0}X^2 + {1}X + {2}=0", a, b, c);
                    Console.WriteLine("Se puede resolver: " + (((b * b) - (4 * a * c)) >= 0));

                    x1 = (-b + Math.Sqrt(b * b - 4 * a * c)) / (2 * a);     //calculo de soluciones
                    x2 = (-b - Math.Sqrt(b * b - 4 * a * c)) / (2 * a);

                    Console.WriteLine("La solución es X1={0} y X2={1}", x1, x2);
                    break;
                case 2:


                    int cantidad;       //cantidad a convertir
                    int bil50, bil20, bil10, bil5, mon2, mon1;  //cantidad de billetes o monedas



                    Console.Write("Introduzca la cantidad de billetes de 20: ");       //lectura 20
                    bil20 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Introduzca el número de billetes de 5: ");       //lectura 5
                    bil5 = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Introduzca el total de las monedas de 2: ");       //lectura 2
                    mon2 = Convert.ToInt32(Console.ReadLine());

                    cantidad = 20 * bil20 + 5 * bil5 + 2 * mon2;
                    Console.WriteLine("\n\nHa introducido {0} billetes de 20, {1} de 5 y {2} monedas de 2e, lo que suma en total {3}euros", bil20, bil5, mon2, cantidad);        //salida de soluciones


                    bil50 = cantidad / 50;                              //calculos
                    bil10 = (cantidad - bil50 * 50) / 10;
                    mon1 = (cantidad - bil50 * 50 - bil10 * 10);

                    Console.WriteLine("\n\n{0}e son {1} billetes de 50, {2} billetes de 10 y {3} monedas de 1", cantidad, bil50, bil10, mon1);        //salida de soluciones


                    break;
                case 3:

                    bool correcto = false;

                    Console.Write("Introduzca el año: ");
                    int anyo = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Introduzca el mes: ");
                    int mes = Convert.ToInt32(Console.ReadLine());


                    if (mes > 0 && mes < 13)
                    {
                        correcto = false;
                        Console.Write("Introduzca el dia: ");
                        int dia = Convert.ToInt32(Console.ReadLine());

                        if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
                        {
                            if (dia > 0 && dia < 32) correcto = true;
                        }
                        else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
                        {
                            if (dia > 0 && dia < 31) correcto = true;
                        }
                        else if ((anyo % 4 == 0 && anyo % 100 != 0) || (anyo % 4 == 0 && anyo % 400 == 0 && anyo % 100 == 0))
                        {
                            if (dia > 0 && dia < 30) correcto = true;
                        }
                        else if (dia > 0 && dia < 29) correcto = true;

                        if (correcto == true)
                        {
                            Console.Write("La fecha es:  {0} de ", dia);

                            switch (mes)
                            {
                                case 1:
                                    Console.Write("enero");
                                    break;
                                case 2:
                                    Console.Write("febrero");
                                    break;
                                case 3:
                                    Console.Write("marzo");
                                    break;
                                case 4:
                                    Console.Write("abril");
                                    break;
                                case 5:
                                    Console.Write("mayo");
                                    break;
                                case 6:
                                    Console.Write("junio");
                                    break;
                                case 7:
                                    Console.Write("julio");
                                    break;
                                case 8:
                                    Console.Write("agosto");
                                    break;
                                case 9:
                                    Console.Write("septiembre");
                                    break;
                                case 10:
                                    Console.Write("octubre");
                                    break;
                                case 11:
                                    Console.Write("noviembre");
                                    break;
                                case 12:
                                    Console.Write("diciembre");
                                    break;


                            }
                            Console.WriteLine(" del {0}.", anyo);

                        }
                        else Console.WriteLine("dia no valido");

                    }
                    else Console.WriteLine("mes no valido");

                    break;

                case 0:
                    break;

                default:
                    Console.WriteLine("\t ERROR: opción inválida");
                    break;
            }

            Console.Write("\n\t Pulse intro para salir"); //salida
            Console.ReadLine();



        }

    }
}

