﻿// Alumno: Leon Oyola, Juan Antonio

/* P01b_Circulo
    Se introduce el radio de un circulo e imprimirá la longitud de su circunferencia (longitud=2*pi*r) y su area (area=pi*r^2)
*/

using System;
using System.Collections.Generic;
using System.Text;

class Program
{
    static void Main(string[] args)
    {
        string lectura;     //string de captura
        double longitud, area;  //resultados
        int r;                  //radio entero
        double pi = 3.141592;   //valor pi

        Console.Write("Introduzca el radio del circulo: ");     //lectura y conversión
        lectura=Console.ReadLine();
        r = Convert.ToInt32(lectura);

        longitud = 2 * pi * r;      //calculos
        area = pi*(r*r);            //no funciona ^2??
              
        Console.WriteLine("La longitud de circunferencia de circulo de radio {0} es {1} y su area {2}", r, longitud, area); //impresion de resultado

    


        Console.Write("\n\n\n\t\tPulsa Intro para salir");
        Console.ReadLine();
    }
}