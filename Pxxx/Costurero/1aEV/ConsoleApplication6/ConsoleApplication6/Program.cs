﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Numero: ");

            int i = 0;
            int num = Convert.ToInt32(Console.ReadLine());
            int aux = 0;

            while (i < 300)
            {
                aux = aux + num;
                Console.Write(aux + "\t");
                i++;
            }

            Console.Write("\n\n\tDO-WHILE: ");
            Console.ReadLine();

            i = 0;
            aux = 0;
            do
            {
                aux = aux + num;
                Console.Write(aux + "\t");
                i++;
            } while (i < 300);


            Console.Write("\n\n\tFOR: ");
            Console.ReadLine();

            aux = 0;
            for(i=0; i < 300; i++)
            {
                aux = aux + num;
                Console.Write(aux + "\t");

            }


            Console.Write("SALIR");
            Console.ReadLine();



        }
    }
}
