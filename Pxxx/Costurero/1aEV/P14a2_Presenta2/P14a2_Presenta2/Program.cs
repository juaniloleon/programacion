﻿/*
Presenta en pantalla, en horizontal separado por tabulador
las siguientes seies de numeros.

1: 500 primeros numeros enteros.
2: los pares hasta 500

Primero con while
despues con do while
por ultimo con for.
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14a1_Presenta1
{
    class Program
    {
        static void Main(string[] args)
        {

            int i = 0;
            Console.WriteLine("A continuación le mostraremos los pares hasta el 500: \n");
            Console.ReadLine();


            while (i < 500)
            {
                Console.Write(i + "\t");
                i=i+2;
            }



            Console.WriteLine("\n\n Pulse intro para verlos otra vez, esta vez los presentaremos con un Do-While \n");
            Console.ReadLine();
            i = 0;
            do
            {
                i=i+2;
                Console.Write(i + "\t");

            }
            while (i < 500);


            Console.WriteLine("\n\n Si lo desea, y si no tambien, se los mostraré con un for \n");
            Console.ReadLine();


            for (i = 0; i < 501; i=i+2)
            {
                Console.Write(i + "\t");

            }

            Console.ReadLine();

        }
    }
}
