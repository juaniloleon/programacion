﻿/*

Alumno: Leon Oyola, Juan Antonio



P21j_DiaDeLaSemana

Se le pide al usuario que introduzca un numero entro 0 y 6, cada uno los cuales
indica el día de la semana en que nos encontramos [0->lunes | 6-> domingo]

Luego pregunta cuantos días quier eavanzar numDias y el programa calcula
de la forma más eficiente en qué día de la semana caerá dentro de numDias

*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P12j_diaDeLaSemana
{
    class Program
    {
        static void Main(string[] args)
        {
            string nombreDiaI = "";
            string nombreDiaF = "";

            Console.Write("Introduzca día de inicio \n 0-> Lunes \n 1-> Martes \n 2-> Miercoles \n 3-> Jueves \n 4-> Viernes \n 5-> Sabado \n 6-> Domingo\n Dia:  ");
            int diaInicio = Convert.ToInt32(Console.ReadLine());
            int suma = 0;

            if (diaInicio >= 0 && diaInicio < 7){
                switch (diaInicio){
                    case 0:
                        nombreDiaI = "Lunes";
                        break;
                    case 1:
                        nombreDiaI = "Martes";
                        break;
                    case 2:
                        nombreDiaI = "Miercoles";
                        break;
                    case 3:
                        nombreDiaI = "Jueves";
                        break;
                    case 4:
                        nombreDiaI = "Viernes";
                        break;
                    case 5:
                        nombreDiaI = "Sabado";
                        break;
                    case 6:
                        nombreDiaI = "Domingo";
                        break;
                }

                Console.Write("Introduca cuantos días quiere avanzar: ");
                int numDias = Convert.ToInt32(Console.ReadLine());

                suma = (numDias + diaInicio) % 7;


                switch (suma)
                {
                    case 0:
                        nombreDiaF = "Lunes";
                        break;
                    case 1:
                        nombreDiaF = "Martes";
                        break;
                    case 2:
                        nombreDiaF = "Miercoles";
                        break;
                    case 3:
                        nombreDiaF = "Jueves";
                        break;
                    case 4:
                        nombreDiaF = "Viernes";
                        break;
                    case 5:
                        nombreDiaF = "Sabado";
                        break;
                    case 6:
                        nombreDiaF = "Domingo";
                        break;
                }


                Console.WriteLine("Si hoy es {0}, dentro de {1} dias será {2}.", nombreDiaI, numDias, nombreDiaF);

            }
            else Console.WriteLine("ERROR_1: Dia incorrecto.");




            Console.Write("Pulse intro para salir: ");
            Console.ReadLine();
        }
    }
}
