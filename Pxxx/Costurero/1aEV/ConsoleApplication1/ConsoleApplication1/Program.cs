﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
static void Main(string[] args)
{
    int edad, peso; //Declaración de 2 variables de tipo entero, edad y peso.

    string nombre;  //Declaración de variable de tipo cadena y nombre nombre.
    string captura;

    edad = 18; // asignación del valor
    edad = edad + 3;
    nombre = "Juanilo";
    peso = 75;

    Console.Write("Por favor, complemente los siguientes datos: \n\nNombre: ");
    nombre = Console.ReadLine();
    Console.WriteLine("Edad: ");
    captura = Console.ReadLine();

    edad = Convert.ToInt32(captura); //-> Conveirte de string a int
                
    Console.WriteLine("Hola " + nombre + ", tienes " + edad + " años de edad pero aparentas muchos menos. \n\n");


    Console.WriteLine("Seguramente no quieras, pero para salir presiona intro.");
    Console.ReadLine();



}
}