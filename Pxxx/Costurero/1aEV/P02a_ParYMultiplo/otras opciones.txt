


            /* //Versión con if en cascada
             bool par = false;           //Banderas de comprobacion
            bool mult = false;

            if (num % 2 == 0)       //rutina de comprobacion
                par = true;
            if (num % 3 == 0)
                mult = true;
                
                                //verion primera
            if (par == true && mult == true)
                Console.WriteLine("El numero {0} es PAR y además es múltiplo de 3", num);
           else  if (par == true && mult == false)
                Console.WriteLine("El numero {0} es PAR y no es múltiplo de 3", num);
            else if (par == false && mult == true)
                Console.WriteLine("El numero {0} es IMPAR y además es múltiplo de 3", num);
            else
                Console.WriteLine("El numero {0} es IMPAR y no es múltiplo de 3", num);

            /*
            if (par == true){
                if (mult == true)
                    Console.WriteLine("El numero {0} es PAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es PAR y no es múltiplo de 3", num);
            }
            else{
                if (mult == true)
                    Console.WriteLine("El numero {0} es IMPAR y además es múltiplo de 3", num);
                else
                    Console.WriteLine("El numero {0} es IMPAR y no es múltiplo de 3", num);
            }*/
            
         
