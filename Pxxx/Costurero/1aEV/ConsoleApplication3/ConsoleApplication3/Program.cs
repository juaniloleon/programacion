﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main(string[] args)
        {

            int cantidad;       //cantidad a convertir
            int bil50, bil20, bil10, bil5, mon2, mon1;  //cantidad de billetes o monedas



            Console.Write("Introduzca la cantidad de billetes de 20: ");       //lectura 20
            bil20 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introduzca el número de billetes de 5: ");       //lectura 5
            bil5 = Convert.ToInt32(Console.ReadLine());

            Console.Write("Introduzca el total de las monedas de 2: ");       //lectura 2
            mon2 = Convert.ToInt32(Console.ReadLine());

            cantidad = 20 * bil20 + 5 * bil5 + 2 * mon2;
            Console.WriteLine("\n\nHa introducido {0} billetes de 20, {1} de 5 y {2} monedas de 2e, lo que suma en total {3}euros", bil20, bil5, mon2, cantidad);        //salida de soluciones


            bil50 = cantidad / 50;                              //calculos
            bil10 = (cantidad - bil50 * 50) / 10;
            mon1 = (cantidad - bil50 * 50 - bil10 * 10);

            Console.WriteLine("\n\n{0}e son {1} billetes de 50, {2} billetes de 10 y {3} monedas de 1", cantidad, bil50, bil10, mon1);        //salida de soluciones


            Console.Write("\n\n\n\t\tPulsa Intro para salir");
            Console.ReadLine();
        }
    }
}
