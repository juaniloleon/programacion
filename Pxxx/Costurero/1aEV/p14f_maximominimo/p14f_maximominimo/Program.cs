﻿/*
Alumno Leon Oyola, Juan Antonio

min<100
max[300,500]

imprime 50 al azar entre min y max*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p14f_maximominimo
{
    class Program
    {
        static void Main(string[] args)
                    {
            var azar = new Random();

            int min = azar.Next(100);
           

            int max = azar.Next(300, 501);
            int num = 0, alto = min, bajo = max;

            for (int i = 0; i < 50; i++)
            {
                num = azar.Next(min, max);
                Console.WriteLine((i+1)+")\t"+num );
                if (num > alto)
                    alto = num;
              if (num < bajo)
                    bajo = num;

            }

            Console.WriteLine("Los valores son: max ({0}) y min ({1})", alto, bajo );

            Console.ReadLine();


        }
    }
}
