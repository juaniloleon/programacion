﻿/*
Leon Oyola, Juan Antonio

P14a5_TrescientosPrimerosMultiplos

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14a5_trescientosmultiplos
{
    class Program
    {
        static void Main(string[] args)
        {

            int i = 0;
            Console.Write("Introduzca un número: ");
            int num = Convert.ToInt32(Console.Read());
            int aux = num;

            while (i <= 300)
            {
                Console.Write(aux + "\t");
                aux = aux + num;
                i++;
            }

            Console.Write("\nAhora con do-while [INTRO] ");
            Console.ReadLine();

            i = 0;
            aux = num;
            do
            {
                Console.Write(aux + "\t");
                aux = aux + num;
                i++;

            } while (i <= 300);
            Console.Write("\nahora con for [INTRO] ");
            Console.ReadLine();

            aux = num;
            for (i = 0; i <= 300; i++)
            {
                Console.Write(aux + "\t");
                aux = aux + num;
                
            }

            Console.Write("\nSalir: [INTRO] ");
            Console.ReadLine();


        }
    }
}
