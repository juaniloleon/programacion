﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplos2
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;


            do
            {
                
                Console.Write("\n\t Introduzca un entero positivo de dos cifras: ");
                num = Convert.ToInt32(Console.ReadLine());
                if (num < 10 || num > 99)
                    Console.WriteLine("\tERROR");
            } while (num < 10 || num > 99);

            /*multiplos de num entre 400 y */

            int mult = (400 / num) * num + num;
            int totalSuma = 0;
            double cont = 0;

            while (mult<700)
            {
                cont++;

                totalSuma += mult;

                Console.Write(mult +"\t");
                mult += num;

                
            }


            Console.WriteLine("\n\n\t La suma total es {0} y la media {1}", totalSuma, totalSuma/cont);



            Console.WriteLine("\n\nSALIENDO...");
            Console.ReadKey();

        }
        
    }
}
