﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P13e_empleos
{
    class Program
    {
        static void Main(string[] args)
        {

            int sueldo = 0;
            string tipo = "";
            bool error = false;

            int euroHora = 0;
            int horasDias = 0;

            //TABLA DE EMPLEADOS

            Console.WriteLine("+----------------------------------------------------------+");
            Console.WriteLine("|   id         Profesión          euros / h      hs / dia  |");
            Console.WriteLine("|    1            Albañil               12           8     |");
            Console.WriteLine("|    2            Gruista               15           7     |");
            Console.WriteLine("|    3            Electricista          16           5     |");
            Console.WriteLine("|    4            Fontanero             14           5     |");
            Console.WriteLine("|    5            Oficinista            10           2     |");
            Console.WriteLine("+----------------------------------------------------------+");



            Console.Write("\n\t¿Qué tipo de empleado necesitas [1..5]?:  ");
            int empleado = Convert.ToInt32(Console.ReadLine());



            switch (empleado)
            {
                case 1:
                    tipo = "albañil";
                    euroHora = 12;
                    horasDias = 8;  //suelo al día trabajado
                    break;
                case 2:
                    tipo = "gruista";
                    euroHora = 15;
                    horasDias=7;  //suelo al día trabajado
                    break;
                case 3:
                    tipo = "electrisista";
                    euroHora = 16;
                    horasDias= 5;  //suelo al día trabajado
                    break;
                case 4:
                    tipo = "fontanero";
                    euroHora = 14;
                    horasDias=4;  //suelo al día trabajado
                    break;
                case 5:
                    tipo = "oficinista";
                    euroHora = 10;
                    horasDias = 2;  //suelo al día trabajado
                    break;
                default:
                    error = true;
                    break;
            }


            if (error == false)
            {

                Console.Write("\n\t¿Cuantos días vas a necesitar a un " + tipo + "?");
                int dias = Convert.ToInt32(Console.ReadLine());

                sueldo = horasDias * euroHora;

                int total = sueldo * dias;
                Console.WriteLine("\n\tUn {0} durante {1} dias son:  {2}", tipo, dias, total);


            }

            else Console.WriteLine("\n\tNo ha introducido una opción correcta.\n\n");

            Console.Write("\n\t\tpulse intro para salir: ");
            Console.ReadLine();
        }
    }
}
