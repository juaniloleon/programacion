﻿/*
Multiplos de num entre 500 y 700
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14_a6_Multiplos_entre_limites
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Numero: ");

           int num = Convert.ToInt32(Console.ReadLine());
            while (num < 10 || num > 99)
            {
                Console.Write("EROR_numero no valido- Numero: ");

                num = Convert.ToInt32(Console.ReadLine());
            }


            int aux = 0;
            int primer;

            if (500 % num == 0)
                primer = 500 ;
           else
                primer = 500 - (500 % num) + num;

            aux = primer;

            Console.Write("\n\n\tWHILE: [INTRO]");
            Console.ReadLine();


            while (aux <= 700)
            {
                Console.Write(aux + "\t");
                aux = aux + num;

            }

            Console.Write("\n\n\tDO-WHILE: [INTRO]");
            Console.ReadLine();


            aux = primer;
            do
            {
                Console.Write(aux + "\t");
                aux = aux + num;

            } while (aux <= 700);


            Console.Write("\n\n\tFOR: [INTRO]");
            Console.ReadLine();

            aux = primer;
            for (aux = primer; aux <= 700; aux += num)
            {
                Console.Write(aux + "\t");

            }


            Console.Write("\n\n\tSALIR");
            Console.ReadLine();



        }
    }
}

