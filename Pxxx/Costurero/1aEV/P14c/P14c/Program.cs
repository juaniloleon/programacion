﻿/* Alumno:  Leon Oyola, Juan Antonio


P14c_ Sumador

EL USUARIO VA INTRODUCIENDO SUMANDOS HASTA QUE SE INTRODUCE EL 0.

AL INTRODUCIR ESTE VALOR EL PROGRAMA DEVUELVE LA SUMA DE LOS NUMEROS INTRODUCIDOS HASTA 
EL MOMENTO Y LA MEDIA.

*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P14c_sumador1
{
    class Program
    {
        static void Main(string[] args)
        {
            int total = 0, valor;
            Console.WriteLine("Introduzca el 0 para acabar");
            double cont = 0;


            do
            {
                Console.Write("Introduzca valor ({0}): ", cont + 1);
                valor = Convert.ToInt32(Console.ReadLine());
                if (valor != 0)
                {
                    cont++;

                    total += valor;
                }


            } while (valor != 0);

            double media = total / cont;

            Console.WriteLine("La suma de los números es {0}, y su media {1}", total, media);
            Console.ReadLine();

        }
    }
}
