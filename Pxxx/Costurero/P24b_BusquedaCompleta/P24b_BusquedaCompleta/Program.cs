﻿/*
 Alumno: Leon Oyola, Juan Antonio
  
 P24B Avanzado

Pide al ususario el tamaño de una tabla de enteros [5...100].Se contruye la tabla de enteros tabEnt
con dicho tamaño.Luego se rellena con valores aleatorios de dos cifras.
A continuación se pide al ususario el numero a buscar y el programa presenta todas las posiciones donde
se presenta dicho número, o lanza el mensaje de "El numero X no éxiste en la tabla".
Esto se repetirá hasta que busquemos el número 0.
    Nota: por supuesto usaremos capturaEntero.
    Avanzado: en lugar de salir con el valor cero, se 
    sale pulsando intro sin ningun valor. 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace p24b_BusquedaCompleta
{

    class Program
    {
        private static int CapturaEntero(string texto, int min, int max)
        {
            int num;
            bool esCorrecto;
            do
            {
                Console.Write("\n\t {0} [{1}..{2}]: ", texto, min, max);
                esCorrecto = Int32.TryParse(Console.ReadLine(), out num);
                Console.ForegroundColor = ConsoleColor.Red;
                if (!esCorrecto)
                    Console.Write("\n\t * ERROR de FORMATO *");
                else if (num < min || num > max)
                {
                    Console.Write("\n\t * ERROR: VALOR FUERA DE RANGO *");
                    esCorrecto = false;
                    num = 0;
                }
                Console.ForegroundColor = ConsoleColor.White;
            } while (!esCorrecto);

            return num;
        }

        private static int CapturaEntero(int min, int max, string entrada)
        {
            int num;
            bool esCorrecto;

            esCorrecto = Int32.TryParse(entrada, out num);
            Console.ForegroundColor = ConsoleColor.Red;
            if (!esCorrecto)
                Console.Write("\n\t * ERROR de FORMATO *");
            else if (num < min || num > max)
            {                
                Console.Write("\n\t * ERROR: VALOR FUERA DE RANGO [{0}...{1}] *", min, max);
                esCorrecto = false;
            }
            Console.ForegroundColor = ConsoleColor.White;
            return num;
        }
        
        private static void MuestraTabla(int[] tabla)
        {
            for (int i = 0; i < tabla.Length; i++)
                Console.WriteLine("{0}) {1}", i, tabla[i]);
        }

        private static bool BuscaImprime(int[] tabla, int valor)
        {
            bool encontrado = false;
            bool primera = true;
            bool localizado = false;
            for (int i = 0; i < tabla.Length; i++)
            {
                encontrado = false;
                if (tabla[i] == valor)
                    encontrado = true;
                if (encontrado && primera)
                {
                    Console.Write("\nEl número {0} está en la/s posicion/es: ", valor);
                    primera = false;
                    localizado = true;
                }
                if (encontrado)
                    Console.Write(i + " ");
                
            }
            return localizado;
        }

        private static int[] RellenaTabla(int[] tabla, int max)
        {
            Random azar = new Random();
            int num = 0;

            //num = CapturaEntero("\tnumero elementos a introducir:", 1, max + 1);
            //for (int i = 0; i < num; i++)
            //    tabla[i] = CapturaEntero("Introduzca valor de tabla:", 10, 99);
            //Console.WriteLine("Se rellenará de forma aleatoria el resto.");

            for (int i = num; i < tabla.Length; i++)
                tabla[i] = azar.Next(10, 100);

            return tabla;
        }

        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;

            bool saliendo = false, encontrado;
            string entrada;

            int valor, tamanyo = CapturaEntero("Tamaño de la tabla:", 5, 100);
            int[] tabla = new int[tamanyo];

            tabla = RellenaTabla(tabla, (tamanyo - 1));
            MuestraTabla(tabla);
            
            do
            {
                encontrado = false;
                Console.Write("\n\n\tIntroduzca valor a buscar [para salir no introduzca nada]: ");
                entrada = Console.ReadLine();

                if (entrada != "")
                {
                    valor = CapturaEntero(10, 99, entrada);
                    encontrado = BuscaImprime(tabla, valor);

                    if (!encontrado && (valor > 9 && valor < 100))
                    {
                        Console.Write("\nEl valor {0}", valor);
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(" NO ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("existe en la tabla");
                    }
                    
                }
                else saliendo = true;


            } while (!saliendo);
        }
    }
}