﻿/*
 COLECCION DE FUNCIONES TW (TO-WAPAS)

 JUANILOLEON
 2017
 
 */
/*
  Rec:

  Dev:

  Función:
  */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
	
	/*
    Rec: entero, entero, entero
    
    Dev: double o float

    Función: calcula la media de 3 numeros 
    */
	
	private static double MediaDeTres(int a, int b, int c){
		double media = (a+b+c)/3.0;
		return Math.Round(media,2);
	}
	private static float MediaDeTres(int a, int b, int c){
		float media = (a+b+c)/3.0;
		return Math.Round(media,2);
	}
	
	
    /*
    Rec: Texto, entero, entero
    
    Dev: Entero

    Función: Captura un entero por teclado, con verificación por TryParte    
                Comprueba que esta en un rango valido
    */
    private static int CapturaEntero(string texto, int min, int max)
    {
        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("\n\t{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n** Error: Valor introducido no válido **");
            else if (resultado < min || resultado > max)
            {
                Console.WriteLine("\n** Error: El número no está en el rango pedido **");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        return resultado;
    }
    /*
   Rec: Texto,

   Dev: Entero

   Función: Captura un entero por teclado, con verificación por TryParte              
   */
    private static int CapturaEntero(string texto)
    {
        bool esCorrecto;
        int resultado;
        string entrada;
		do
        {
            Console.Write("\n\t{0}: ", texto);
            
			entrada=Console.ReadLine();
			if(entrada!="\n"){			
			
				esCorrecto = Int32.TryParse(entrada, out resultado);
				if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
					Console.WriteLine("\n** Error: Valor introducido no válido **");
			}
			else return -1;
			
				
				
			
        } while (esCorrecto == false);

        return resultado;
    }
	
	
    /*
 Rec: int

 Dev: boolean

 Función: recibe un numero y devuelve true si es primo            
 */

    public static bool EsPrimo(int num)
    {

        int contador = 2;
        bool primo = true;
        while ((primo) && (contador != num))
        {
            if (num % contador == 0)
                primo = false;
            contador++;
        }
        return primo;
    }

    /*
Rec: int

Dev: long

Función: recibe un numero y devuelve su factorial     
*/
    private static long Factorial(int num)
    {
        long factorial = 1;
        for (int i = 2; i <= num; i++)
        {
            factorial *= i;
        }
        return factorial;

    }

    /*
Rec: int, bool

Dev: nada

Función: recibe un numero y lo coloca en su sitio (tabla 10x10) 
            si recibe true lo imprime en un color diferente
*/


    private static void ColocaElNumero(int num, bool color)
    {
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Red;
        }

        int decena = num / 10;
        int columna = 3 + 5 * (num - decena * 10);
        int fila = 2 + 2 * decena;

        Console.SetCursorPosition(columna, fila);
        Console.Write(num);
        if (color)
        {
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }

    /*
Rec: int,

Dev: bool

Función: recibe un numero y comprueba si es el buscado o no (NECESITA VARIABLE GLOBAR ACIERTA)
            si se ha equivocado incremeta el contador (NECESITA GLOBAL CONT)
            si ha acertado devuelve un true.
*/

    private static int acierta = 0;
    private static int cont = 0;
    private static bool CompruebaTiro(int num)
    {
        bool correcto = false;
        Console.SetCursorPosition(4, 21);
        if (num == acierta)
        {

            Console.Clear();
            Console.SetCursorPosition(4, 5);

            Console.WriteLine("\t Has acertado en {0} intentos", cont);
            correcto = true;
        }
        else if (num < acierta)
            Console.Write("Te has quedado corto");
        else
            Console.Write("Te has pasado\t\t");
        return correcto;
    }


   
}
