﻿//Primero se pide un mínimo(0<min<=10) y un máximo (min<max<100).Luego, la máquina elige un valor al azar entre 
//estos dos(min<=numOK<=max).
//A continuación se pide al usuario que acierte el número oculto. cada fallo le indica si se ha pasado o se ha quedado corto.
//Cuando acierte escribirá:"Has acertado en x intentos".
//Tienes que utilizar los métodos siguientes:
//CapturaEntero, para introducir los números.
//CompruebaTiro, que recibe el número que ha puesto el usuario, responde si ha acertado o se ha quedado corto o se ha pasado,
//devuelve un booleano true si ha acertado o false si no.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
class Program
{
    static int CapturaEntero(string frase, int min, int max)
    {

        int resultado;
        bool esCorrecto;
        do
        {
            Console.Write(frase + "[ " + min + " hasta " + max + "] : ");
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
                Console.WriteLine("\n ERROR: El valor que has introducido no es válido");

            if (resultado > max || resultado < min)//compruebo el error del maximo y minimo
            {
                Console.WriteLine("\n ERROR: El número que has introducido no está en el rango");
                esCorrecto = false;
            }


        } while (!esCorrecto || resultado > max || resultado < min);

        return resultado;
    }

    static void MuestraTabla(int[] tabla)
    {
        // Mostremos todos los elementos de la tabla
        for (int i = 0; i < tabla.Length; i++)
        {
            // mostramos cada posición con su valor
            Console.WriteLine("[{0}] {1}", i, tabla[i]);
        }
        //foreach (int e in tabla)
        //{
        //    Console.WriteLine("{0}) ", e);
        //}

    }
    static void Main(string[] args)
    {

        int[] tablaEnteros;

        int tamanyo = 10;
        //int tamanyo = CapturaEntero("número de elementos del vector: ", 10, 99);
        tablaEnteros = new int[tamanyo];

        for (int i = 0; i < 4; i++)
            tablaEnteros[i] = CapturaEntero("\n\t¿Un número? ", 0, 99);


        Random azar = new Random();
        for (int i = 4; i < tablaEnteros.Length; i++)

            tablaEnteros[i] = azar.Next(10, 100);
        MuestraTabla(tablaEnteros);

        int id= CapturaEntero("Una posición: ", 0, tamanyo - 1);
        while (id != 0)
        {
           
            tablaEnteros[id] = CapturaEntero("\n\t¿Un número? ", 0, 99);
            id = CapturaEntero("Una posición: ", 0, tamanyo - 1);
        }
        MuestraTabla(tablaEnteros);

        bool existe;
        int valor;
        do
        {
            existe = false;
            valor = CapturaEntero("Valor a buscar", 0, 100);
            for (int i=0; i<=(tablaEnteros.Length-1); i++)
            {
                if (valor == tablaEnteros[i])
                {
                    Console.WriteLine("{0} está en tabla[{1}]", valor, i);
                    existe = true;
                }
                
            }
            if (!existe)
                Console.WriteLine("No existe ese valor");
           

        } while (!existe);


        Console.WriteLine("\n\nSaliendo...");
        Console.ReadKey(true);

    }
}
