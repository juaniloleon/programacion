﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

class Program
{
    static int[,] tabPrim;
    static void Main(string[] args)
    {
        LeeDatos("Primitivas08");
        ImprimeTabla();

        ConsultaRep();
        Frecuencias();
        LaMasBaja();

        Pausa("salir");
        Console.ReadKey(true);
    }

    private static void LaMasBaja()
    {
        int total;
        int max = 0;
        int pos = 0;

        for (int i = 1; i < 50; i++)
        {
            total = 0;

            for (int k = 2; k < tabPrim.GetLength(1)-2; k++)
                total += tabPrim[i, k];
            if (total > max)
                pos = i;

        }
        Console.ForegroundColor = ConsoleColor.White;

        Console.WriteLine("La suma más baja es:\n\tFecha\tNumeros agraciados en el día señalado\t\tC\tR\n");

        Console.ForegroundColor = ConsoleColor.Red;

        Console.Write("\t{0}-{1}: ", tabPrim[pos, 0], tabPrim[pos, 1]);
        for (int j = 2; j < tabPrim.GetLength(1); j++)
            Console.Write("\t" + tabPrim[pos, j]);
        Console.WriteLine();


    }

    private static void Frecuencias()
    {
        int cont;

        for (int i = 1; i < 50; i++)
        {
            if (i % 2 == 0)
                Console.ForegroundColor = ConsoleColor.Green;
            else
                Console.ForegroundColor = ConsoleColor.Yellow;
            cont = 0;

            for (int l = 0; l < tabPrim.GetLength(0); l++)
                for (int k = 2; k < tabPrim.GetLength(1); k++)
                    if (i == tabPrim[l, k])
                        cont++;

            if (cont == 0)
                Console.WriteLine("\tEl numero {0} no tiene apariciones\n", i);
            else
                Console.WriteLine("\tEl numero {0} tiene {1} repeticiones.\n", i, cont);
        }


    }


    private static void ConsultaRep()
    {
        int nume = CapturaEntero("numero a buscar", 0, 49);
        int cont;
        if(nume!=0)
        do
        {
            cont = 0;

            for (int l = 0; l < tabPrim.GetLength(0); l++)
                for (int k = 2; k < tabPrim.GetLength(1); k++)
                    if (nume == tabPrim[l, k])
                        cont++;



            if (cont == 0)
                Console.WriteLine("El numero {0} no ha sido encontrado.", nume);
            else
                Console.WriteLine("El numero {0} tiene {1} repeticiones.", nume, cont);

            nume = CapturaEntero("numero a buscar", 0, 49);
        } while (nume != 0);
    }

    private static void ImprimeTabla()
    {
        Console.WriteLine("\tFecha\tNumeros agraciados en el día señalado\t\tC\tR\n");
        for (int i = 0; i < tabPrim.GetLength(0); i++)
        {
            Console.Write("\t{0}-{1}: ", tabPrim[i, 0], tabPrim[i, 1]);
            for (int j = 2; j < tabPrim.GetLength(1); j++)
                Console.Write("\t" + tabPrim[i, j]);
            Console.WriteLine();

        }


    }

    private static int[,] LeeDatos(string archivo)
    {
        string[] aux = new string[8];

        List<string> lista = new List<String>();

        StreamReader sr = new StreamReader(archivo + ".txt");
        string linea;

        do
        {
            linea = sr.ReadLine();
            if (linea != null)
                lista.Add(linea);

        }
        while (linea != null);

        sr.Close();
        tabPrim = new int[lista.Count, 10];


        for (int i = 0; i<lista.Count; i++)
        {
            aux = lista[i].Split(';');
            for (int j = 0; j < tabPrim.GetLength(1); j++)
                tabPrim[i, j] = Convert.ToInt32(aux[j]);
        }


        return tabPrim;
    }

    private static int CapturaEntero(string texto, int min, int max)
    {
        Console.ForegroundColor = ConsoleColor.White;

        bool esCorrecto;
        int resultado;
        do
        {
            Console.Write("{0} [{1}..{2}]: ", texto, min, max);
            esCorrecto = Int32.TryParse(Console.ReadLine(), out resultado);
            if (esCorrecto == false)// <-- Compruebo si hemos introducido un entero
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Pausa("continuar [valor no valido]");

            }
            else if (resultado < min || resultado > max)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Pausa("continuar [valor fuera de rango]");
                esCorrecto = false;
            }
        } while (esCorrecto == false);

        Console.ForegroundColor = ConsoleColor.Gray;

        return resultado;
    }

    private static void Pausa(string texto)
    {
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write("Pulsa una tecla para " + texto + "\t\t\t");
        Console.ForegroundColor = ConsoleColor.Gray;

        Console.ReadKey(true);


    }
}
